import torch
from tqdm import tqdm
from pathlib import Path

from src.data_processing import data_loading
from src.pipeline import logger as training_logger
from src.model import architecture
from src.model import classifiers


class RepresentationClassificationPipeline:
    def __init__(self, repr_model, classifier, criterion, optimizer_repr, optimizer_clf, dataloader, test_dataloader, logger, config):
        self.repr_model = repr_model
        self.classifier = classifier
        self.criterion = criterion
        self.optimizer_repr = optimizer_repr
        self.optimizer_clf = optimizer_clf
        self.dataloader = dataloader
        self.test_dataloader = test_dataloader
        self.logger = logger
        self.config = config

    def train_model(self):
        for epoch in range(self.config.epochs):
            self.run_epoch(epoch)
            self.run_test_epoch(epoch)

    def run_epoch(self, epoch):
        acc = 0
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            representation = self.repr_model(x)
            y_hat = self.classifier(representation)

            self.classifier.zero_grad()
            loss = self.criterion(y_hat, y)
            loss.backward()
            self.optimizer_clf.step()
            self.optimizer_repr.step()

            self.logger(epoch, loss, step)
            acc += (y == y_hat.max(dim=1)[1]).float().mean()
        print(f"train acc {acc / len(self.dataloader)}")
        self.save_model(epoch)

    def run_test_epoch(self, epoch):
        acc = 0
        with torch.no_grad():
            for step, (x, y) in tqdm(enumerate(self.test_dataloader)):
                x, y = x.to(device=self.config.device), y.to(device=self.config.device)
                representation = self.repr_model(x)
                y_hat = self.classifier(representation)
                acc += (y == y_hat.max(dim=1)[1]).float().mean()
            print(f"test acc {acc / len(self.dataloader)}")

    def save_model(self, epoch):
        if (epoch % self.config.save_model_interval == 0) and epoch:
            save_folder = Path(self.config.save_model_path.format(
                    ds_name=self.config.ds_name,
                    model_architecture=self.config.model_architecture,
                    hparams=self.config.hparams_str,
            ))
            save_folder.mkdir(parents=True, exist_ok=True)
            save_path = str(save_folder / f"checkpoint_repr_classifier_repr_{epoch}.pth")
            torch.save(self.classifier.state_dict(), save_path)

    @classmethod
    def from_config(cls, arch, data_path, config):
        config.device = torch.device(config.device)
        repr_model = arch.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.repr_checkpoint_path)
        repr_model.load_state_dict(checkpoint, strict=True)
        repr_model = repr_model.encoder
        repr_model = repr_model.cuda()
        # repr_model = repr_model.eval()
        classifier = classifiers.ClassifierHead.from_config(config).to(device=config.device)
        # model = architecture.ReprClf(head=classifier, repr_model=repr_model)

        dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="train")
        test_dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config,
                                                                       ds_type="test")
        criterion = torch.nn.CrossEntropyLoss()
        optimizer_repr = torch.optim.Adam(repr_model.parameters(), lr=config.clf_lr / 20, betas=config.betas)
        optimizer_clf = torch.optim.Adam(classifier.parameters(), lr=config.clf_lr, betas=config.betas)
        logger = training_logger.ClfLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            repr_model=repr_model,
            classifier=classifier,
            criterion=criterion,
            optimizer_repr=optimizer_repr,
            optimizer_clf=optimizer_clf,
            dataloader=dataloader,
            test_dataloader=test_dataloader,
            logger=logger,
            config=config,
        )


class ClassificationPipeline:
    def __init__(self, classifier, criterion, optimizer, databunch, logger, config):
        self.classifier = classifier
        self.criterion = criterion
        self.optimizer = optimizer
        self.databunch = databunch
        self.logger = logger
        self.config = config
        self.downsample = torch.nn.AvgPool2d(kernel_size=3, stride=2, padding=1)

    def train_model(self):
        for epoch in range(self.config.epochs):
            self.run_epoch(epoch, mode="train")
            self.run_epoch(epoch, mode="test")

    def run_epoch(self, epoch, mode):
        acc = 0
        for step, (x, y) in tqdm(enumerate(self.databunch[mode])):
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            x = x if self.config.upsample_img else self.downsample(x)
            y_hat = self.classifier(x)
            if mode == "train":
                self.classifier.zero_grad()
                loss = self.criterion(y_hat, y)
                loss.backward()
                self.optimizer.step()
                self.logger(epoch, loss, step)
            acc += (y == y_hat.max(dim=1)[1]).float().mean()
        print(f"{mode} acc {acc / len(self.databunch[mode])}")
        self.save_model(epoch)

    def save_model(self, epoch):
        if (epoch % self.config.save_clf_interval == 0) and epoch:
            save_folder = Path(self.config.save_model_path.format(
                ds_name=self.config.ds_name,
                model_architecture=self.config.model_architecture,
                hparams=self.config.hparams_str,
            ))
            save_folder.mkdir(parents=True, exist_ok=True)
            save_path = str(save_folder / f"checkpoint_classifier_{epoch}.pth")
            torch.save(self.classifier.state_dict(), save_path)

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        databunch = data_loading.Databunch.from_config(config, data_path)
        classifier = classifiers.ResNet.from_config(config).to(device=config.device)
        classifier = classifier.cuda()

        criterion = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(classifier.parameters(), lr=config.clf_lr, betas=config.betas)
        logger = training_logger.ClfLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            classifier=classifier,
            criterion=criterion,
            optimizer=optimizer,
            databunch=databunch,
            logger=logger,
            config=config,
        )
