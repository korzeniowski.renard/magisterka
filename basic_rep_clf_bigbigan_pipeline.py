import argparse
import torch
from torchvision import datasets
from torch.utils.data import DataLoader
import torchvision.transforms as transforms


def get_cifar10(args, data_dir='./data/cifar/'):
    """Returning cifar dataloder."""
    transform = transforms.Compose([transforms.Resize(32), #3x32x32 images.
                                    transforms.CenterCrop(32),
                                    transforms.ToTensor()])
    data = datasets.CIFAR10(root=data_dir, train=True, download=True, transform=transform)
    dataloader = DataLoader(data, batch_size=args.batch_size, shuffle=True)
    return dataloader


CHECKPOINT_PATH = "/home/ec2-user/renard/bigbigan/data/CIFAR10/bigbigan/checkpoints/checkpoint_150.pth"

parser = argparse.ArgumentParser()
parser.add_argument("--num_epochs", type=int, default=200,
                    help="number of epochs")
parser.add_argument('--lr_adam', type=float, default=1e-4,
                    help='learning rate')
parser.add_argument('--lr_rmsprop', type=float, default=1e-4,
                    help='learning rate RMSprop if WGAN is True.')
parser.add_argument("--batch_size", type=int, default=128,
                    help="Batch size")
parser.add_argument('--latent_dim', type=int, default=256,
                    help='Dimension of the latent variable z')
parser.add_argument('--wasserstein', type=bool, default=False,
                    help='If WGAN.')
parser.add_argument('--clamp', type=float, default=1e-2,
                    help='Clipping gradients for WGAN.')
parser.add_argument('--start_epoch', type=int, default=0,
                    help='Start epoch based on checkpoint.')
parser.add_argument('--checkpoint', type=str, default=CHECKPOINT_PATH,
                    help='Path to checkpoint.')

#parsing arguments.
args = parser.parse_args()

epochs = 200
class_count = 10
representation_dim = 200


def calculate_acc(preds, labels):
    preds = preds.max(dim=1)[1].numpy()
    labels = labels.numpy()
    return sum(preds == labels) / len(labels)


def train_model(data, epochs, encoder, device):
    for epoch in range(epochs):
        running_acc = 0
        best_acc = 0
        for i, (x, y) in enumerate(data):
            x = downsample(x)
            x = x.to(device)
            optimizer.zero_grad()
            with torch.no_grad():
                enc_x = encoder(x)

            outputs = net(enc_x).cpu()
            loss = criterion(outputs, y)
            loss.backward()
            optimizer.step()

            running_acc += calculate_acc(preds=outputs, labels=y)
            if i % 250 == 0:
                print(f"epoch {epoch + 1}, batch {i + 1}, loss {loss}")
                print(f"accuracy {running_acc / (i + 1)}")
        # if epoch % 50:
        #     torch.save(net.state_dict(), "repr" + CHECKPOINT_PATH.format(epoch))

        best_acc = max(best_acc, running_acc)
        print(f"best_acc {best_acc / (i + 1)}")


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
net = torch.nn.Sequential(
    torch.nn.Linear(representation_dim, representation_dim),
    torch.nn.ReLU(),
    # torch.nn.Linear(representation_dim, representation_dim // 2),
    # torch.nn.ReLU(),
    torch.nn.Linear(representation_dim, class_count),
    torch.nn.Sigmoid(),
).to(device)

criterion = torch.nn.CrossEntropyLoss(reduce='sum')
optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

data = get_cifar10(args)

from src.model import architecture
from src.training_utils import training_utils
config = training_utils.get_config("CIFAR10")
config.device = torch.device(config.device)
repr_model = architecture.BigBiGAN.from_config(config).to(device=config.device)
checkpoint = torch.load(CHECKPOINT_PATH)
repr_model.load_state_dict(checkpoint, strict=True)
repr_model = repr_model.encoder
repr_model = repr_model.cuda()
repr_model = repr_model.eval()
encoder = repr_model

downsample = torch.nn.AvgPool2d(kernel_size=3, stride=2, padding=1)

train_model(epochs=epochs, data=data, encoder=encoder, device=device)
