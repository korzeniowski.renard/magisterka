import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision.utils as vutils
from tqdm import tqdm
from pathlib import Path

from src.data_processing import data_loading
from src.pipeline import logger as training_logger
from src.model import adversarial
from src.model import architecture
from src.model import losses
from src.training_utils import training_utils as tu

class Pipeline:
    def __init__(
            self, dataloader, model, gen_criterion, disc_criterion,
            gen_optimizer, disc_optimizer, logger, config
    ):
        self.dataloader = dataloader
        self.model = model
        self.gen_criterion = gen_criterion
        self.disc_criterion = disc_criterion
        self.gen_optimizer = gen_optimizer
        self.disc_optimizer = disc_optimizer
        self.logger = logger
        self.config = config
        self.counter = 0
        self.downsample = torch.nn.AvgPool2d(kernel_size=3, stride=2, padding=1)

    def train_model(self):
        for epoch in range(self.config.epochs):
            self.counter = 0
            self.run_epoch(epoch)

    def save_model(self, epoch):
        if (epoch % self.config.save_model_interval == 0) and epoch:
            save_folder = Path(self.config.save_model_path.format(
                    ds_name=self.config.ds_name,
                    model_architecture=self.config.model_architecture,
                    hparams=self.config.hparams_str,
            ))
            save_folder.mkdir(parents=True, exist_ok=True)
            save_path = str(save_folder / f"checkpoint_{epoch}.pth")
            torch.save(self.model.state_dict(), save_path)

    def save_img(self, epoch, real_img, img_gen, latent=None, y=None):
        if epoch % self.config.save_metric_interval == 0 and self.counter == 0:
            with torch.no_grad():
                #img_gen, noise = self.model.generate_imgs(fixed=True)
                fake = img_gen.detach().cpu()[:self.config.save_img_count, ...]
            fake_img = np.transpose(vutils.make_grid(
                fake, padding=2, nrow=self.config.img_rows, normalize=True), (1, 2, 0))
            plt.imshow(fake_img)

            file_name = f"ep{epoch}_step{self.counter}.png"
            gen_imgs_save_folder = Path(self.config.gen_imgs_save_path.format(
                ds_name=self.config.ds_name,
                model_architecture=self.config.model_architecture,
                hparams=self.config.hparams_str,
            ))
            gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
            gen_imgs_save_path = str(gen_imgs_save_folder / file_name)
            plt.savefig(fname=gen_imgs_save_path)

            if latent is not None:
                img_gen, noise = self.model.generate_imgs(cls=y, noise=latent)
                img_gen = img_gen.detach().cpu()[:self.config.save_img_count, ...]
                img_gen = np.transpose(vutils.make_grid(
                    img_gen, padding=2, nrow=self.config.img_rows, normalize=True), (1, 2, 0))
                plt.imshow(img_gen)

                file_name = f"ep{epoch}_step{self.counter}_reconstructed.png"
                gen_imgs_save_folder = Path(self.config.gen_imgs_save_path.format(
                    ds_name=self.config.ds_name,
                    model_architecture=self.config.model_architecture,
                    hparams=self.config.hparams_str,
                ))
                gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
                gen_imgs_save_path = str(gen_imgs_save_folder / file_name)
                plt.savefig(fname=gen_imgs_save_path)

            # real_img = real_img.cpu()[:self.config.save_img_count, ...]
            # real_img = np.transpose(vutils.make_grid(
            #     real_img, padding=2, nrow=self.config.img_rows, normalize=True), (1, 2, 0))
            # plt.imshow(real_img)
            # real_imgs_save_folder = Path(self.config.real_imgs_save_path.format(
            #     ds_name=self.config.ds_name,
            #     model_architecture=self.config.model_architecture,
            #     hparams=self.config.hparams_str,
            # ))
            # real_imgs_save_folder.mkdir(parents=True, exist_ok=True)
            # real_imgs_save_path = real_imgs_save_folder / file_name
            # plt.savefig(
            #     fname=str(real_imgs_save_path),
            # )

        self.counter += 1


class AbsBigBiGANPipeline(Pipeline):
    def __init__(self, config, enc_optimizer, *args, **kwargs):
        super().__init__(config=config, *args, **kwargs)
        self.enc_optimizer = enc_optimizer

    def get_logging_dict(self):
        return {
            "postdisc_loss": None,
            "postgen_loss": None,
            "disc_real_latent_acc_postdisc": 0,
            "disc_real_img_acc_postdisc": 0,
            "disc_real_comb_acc_postdisc": 0,
            "disc_real_latent_acc_postgen": 0,
            "disc_real_img_acc_postgen": 0,
            "disc_real_comb_acc_postgen": 0,
            "disc_gen_latent_acc_postdisc": 0,
            "disc_gen_img_acc_postdisc": 0,
            "disc_gen_comb_acc_postdisc": 0,
            "disc_gen_latent_acc_postgen": 0,
            "disc_gen_img_acc_postgen": 0,
            "disc_gen_comb_acc_postgen": 0,
        }

    def log_statistics(self, loss, logging_dict, outputs, update_type):
        logging_dict[f"{update_type}_loss".replace("post", "")] = loss
        logging_dict[f"disc_real_latent_acc_{update_type}"] = outputs["img_real_score"].mean()
        logging_dict[f"disc_real_img_acc_{update_type}"] = outputs["img_gen_score"].mean()
        logging_dict[f"disc_real_comb_acc_{update_type}"] = outputs["comb_real_score"].mean()
        logging_dict[f"disc_gen_latent_acc_{update_type}"] = outputs["z_noise_score"].mean()
        logging_dict[f"disc_gen_img_acc_{update_type}"] = outputs["img_gen_score"].mean()
        logging_dict[f"disc_gen_comb_acc_{update_type}"] = outputs["comb_gen_score"].mean()


class BiGANPipeline(Pipeline):
    def __init__(self, fixed_z, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fixed_z = fixed_z
        self.counter = 0

    def run_epoch(self, epoch):
        ge_losses = 0
        d_losses = 0
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x = x if self.config.upsample_img else self.downsample(x)

            y_true = torch.autograd.Variable(torch.ones((x.size(0), 1)).to(self.config.device))
            y_fake = torch.autograd.Variable(torch.zeros((x.size(0), 1)).to(self.config.device))

            # Noise for improving training.
            noise1 = torch.autograd.Variable(torch.Tensor(x.size()).normal_(0,
                                                             0.1 * (self.config.epochs - epoch + 1) / (
                                                                         self.config.epochs + 1)),
                              requires_grad=False).to(self.config.device)
            noise2 = torch.autograd.Variable(torch.Tensor(x.size()).normal_(0,
                                                             0.1 * (self.config.epochs - epoch + 1) / (
                                                                         self.config.epochs + 1)),
                              requires_grad=False).to(self.config.device)

            # Cleaning gradients.
            self.disc_optimizer.zero_grad()

            # Generator:
            z_fake = torch.autograd.Variable(torch.randn((x.size(0), self.config.bigan_latent, 1, 1)).to(self.config.device),
                              requires_grad=False)
            x_fake = self.model.G(z_fake)

            # Encoder:
            x_true = x.float().to(self.config.device)
            z_true = self.model.E(x_true)

            # Discriminator
            out_true = self.model.D(x_true + noise1, z_true)
            out_fake = self.model.D(x_fake + noise2, z_fake)

            # Losses
            if self.config.wasserstein:
                loss_d = - torch.mean(out_true) + torch.mean(out_fake)
            else:
                loss_d = self.disc_criterion(out_true, y_true) + self.disc_criterion(out_fake, y_fake)

            # Computing gradients and backpropagate.
            loss_d.backward()
            self.disc_optimizer.step()

            # Cleaning gradients.
            self.gen_optimizer.zero_grad()

            # Generator:
            z_fake = torch.autograd.Variable(torch.randn((x.size(0), self.config.bigan_latent, 1, 1)).to(self.config.device),
                              requires_grad=False)
            x_fake = self.model.G(z_fake)

            # Encoder:
            x_true = x.float().to(self.config.device)
            z_true = self.model.E(x_true)

            # Discriminator
            out_true = self.model.D(x_true + noise1, z_true)
            out_fake = self.model.D(x_fake + noise2, z_fake)

            # Losses
            if self.config.wasserstein:
                loss_ge = - torch.mean(out_fake) + torch.mean(out_true)
            else:
                loss_ge = self.disc_criterion(out_fake, y_true) + self.disc_criterion(out_true, y_fake)

            loss_ge.backward()
            self.gen_optimizer.step()

            if self.config.wasserstein:
                for p in self.model.D.parameters():
                    p.data.clamp_(-self.config.clamp, self.config.clamp)

            ge_losses += loss_ge.item()
            d_losses += loss_d.item()

        self.save_model(epoch)

        print("Training... Epoch: {}, Discrimiantor Loss: {:.3f}, Generator Loss: {:.3f}".format(
            epoch, d_losses / len(self.dataloader), ge_losses / len(self.dataloader)
        ))

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        model = architecture.BiGAN(config)
        fixed_z = torch.autograd.Variable(torch.randn((16, config.bigan_latent, 1, 1)),
                           requires_grad=False).to(config.device)
        disc_criterion = torch.nn.BCELoss()
        if config.wasserstein:
            gen_enc_optimizer = torch.optim.RMSprop(list(model.G.parameters()) +
                                         list(model.E.parameters()), lr=config.lr_gen)
            disc_optimizer = torch.optim.RMSprop(model.D.parameters(), lr=config.lr_disc)
        else:
            gen_enc_optimizer = torch.optim.Adam(list(model.G.parameters()) +
                                      list(model.E.parameters()), lr=config.lr_gen)
            disc_optimizer = torch.optim.Adam(model.D.parameters(), lr=config.lr_disc)

        logger = training_logger.BiGANLogger.from_config(config=config, name=config.hparams_str)

        return cls(
            dataloader=dataloader,
            model=model,
            disc_criterion=disc_criterion,
            gen_criterion=None,
            gen_optimizer=gen_enc_optimizer,
            disc_optimizer=disc_optimizer,
            logger=logger,
            config=config,
            fixed_z=fixed_z,
        )


class BigBiGANPipeline(AbsBigBiGANPipeline):
    def run_epoch(self, epoch):
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            small_x = x if self.config.upsample_img else self.downsample(x)

            # fix gradient handling so that gradients can be used without rerunning the whole model
            # check if works with gen_output.detach(), enc_outputs.detach()
            logging_dict = self.get_logging_dict()
            self.model.req_grad_disc(True)
            if step % self.config.gen_steps == 0:
                img_gen, noise = self.model.generate_imgs(cls=y)
                z_img = self.model.generate_latent(img=x)
                self.disc_optimizer.zero_grad()
                outputs = self.model.forward(
                    img_real=small_x,
                    img_gen=img_gen.detach(),
                    z_noise=noise,
                    z_img=z_img.detach(),
                    cls=y
                )
                disc_loss = self.disc_criterion(outputs)
                disc_loss.backward()
                self.disc_optimizer.step()
                self.log_statistics(disc_loss, logging_dict, outputs, "postdisc")

            if step % self.config.disc_steps == 0:
                self.model.req_grad_disc(False)
                self.gen_optimizer.zero_grad()
                self.enc_optimizer.zero_grad()
                img_gen, noise = self.model.generate_imgs(cls=y)
                z_img = self.model.generate_latent(img=x)
                outputs = self.model.forward(img_real=small_x, img_gen=img_gen, z_noise=noise, z_img=z_img, cls=y)
                gen_enc_loss = self.gen_criterion(outputs)
                gen_enc_loss.backward()
                self.gen_optimizer.step()
                self.enc_optimizer.step()
                self.log_statistics(gen_enc_loss, logging_dict, outputs, "postgen")

            self.save_img(epoch=epoch, real_img=x, img_gen=img_gen, latent=z_img, y=y)
            self.save_model(epoch)
            self.logger(epoch=epoch, step=step, logging_dict=logging_dict)

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        model = architecture.BigBiGAN.from_config(config).to(device=config.device)

        gen_enc_criterion = losses.GeneratorEncoderLoss(config.latent_loss)
        disc_criterion = losses.BiDiscriminatorLoss()

        gen_optimizer = torch.optim.Adam(model.get_gen_params(), lr=config.lr_gen, betas=config.betas)
        enc_optimizer = torch.optim.Adam(model.get_enc_params(), lr=config.lr_enc, betas=config.betas)
        disc_optimizer = torch.optim.Adam(model.get_disc_params(), lr=config.lr_disc, betas=config.betas)

        logger = training_logger.BiGANLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            model=model,
            gen_criterion=gen_enc_criterion,
            disc_criterion=disc_criterion,
            gen_optimizer=gen_optimizer,
            enc_optimizer=enc_optimizer,
            disc_optimizer=disc_optimizer,
            dataloader=dataloader,
            logger=logger,
            config=config,
        )


class BigBiGANInference:
    def __init__(self, model, dataloader, config):
        self.model = model
        self.dataloader = dataloader
        self.config = config

    def inference(self):
        for step, (org_img, y) in tqdm(enumerate(self.dataloader)):
            org_img, y = org_img.to(device=self.config.device), y.to(device=self.config.device)
            latent = self.encode(org_img)
            reconstructed_img, noise = self.generate(y, latent)
            self.save_img(org_img, reconstructed_img)
            break

    def encode(self, img):
        z_img = self.model.generate_latent(img=img)
        return z_img

    def generate(self, y, latent):
        img_gen, noise = self.model.generate_imgs(cls=y, noise=latent)
        return img_gen, noise

    def save_img(self, org_img, reconstructed_img):
        for name, img in [("org_img", org_img), ("reconstructed_img", reconstructed_img)]:
            img = img.detach().cpu()[:self.config.save_img_count, ...]
            img = np.transpose(vutils.make_grid(
                img, padding=2, nrow=self.config.img_rows, normalize=True), (1, 2, 0))
            plt.imshow(img)

            file_name = f"{name}.png"
            gen_imgs_save_folder = Path(self.config.rec_imgs_save_path.format(
                ds_name=self.config.ds_name,
                model_architecture=self.config.model_architecture,
                hparams=self.config.hparams_str,
            ))
            gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
            gen_imgs_save_path = str(gen_imgs_save_folder / file_name)
            plt.savefig(fname=gen_imgs_save_path)

    @classmethod
    def from_checkpoint(cls, data_path, checkpoint_path, config):
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        model = architecture.BigBiGAN.from_config(config).to(device=config.device)
        checkpoint = torch.load(checkpoint_path)
        model.load_state_dict(checkpoint, strict=True)
        model = model.cuda()
        model = model.eval()
        return cls(model=model, dataloader=dataloader, config=config)


class GANPipeline(Pipeline):
    def run_epoch(self, epoch):
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            logging_dict = self.get_logging_dict()
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            x = x if self.config.upsample_img else self.downsample(x)
            if self.model.cls is None: self.model.cls = y.detach()

            if step % self.config.gen_steps == 0:
                self.model.req_grad_disc(True)
                disc_loss, disc_real_acc, disc_fake_acc, gen_img = self.forward_disc(x, y)
                logging_dict["Loss/Disc"] = disc_loss
                logging_dict["Disc/RealAcc"] = disc_real_acc
                logging_dict["Disc/FakeAcc"] = disc_fake_acc

            if step % self.config.disc_steps == 0:
                self.model.req_grad_disc(False)
                gen_loss, gen_disc_acc, gen_img = self.forward_gen(y)
                logging_dict["Loss/Gen"] = gen_loss
                logging_dict["Gen/DiscFakeAcc"] = gen_disc_acc

            self.save_img(epoch=epoch, real_img=x, img_gen=gen_img)
            self.save_model(epoch)
            self.logger(epoch, step, logging_dict)

    def forward_gen(self, y):
        self.model.generator.zero_grad()
        gen_img, noise = self.model.generate_imgs(cls=y)
        _, pred_gen_img = self.model.discriminator(x=gen_img, cls=y)
        pred_gen_img = torch.sigmoid(pred_gen_img.reshape(-1))

        label_gen_img = torch.ones(pred_gen_img.shape[0], device=self.config.device)
        gen_loss = self.gen_criterion(pred_gen_img, label_gen_img)
        gen_loss.backward()

        gen_disc_acc = 1 - pred_gen_img.mean().item()
        self.gen_optimizer.step()

        return gen_loss, gen_disc_acc, gen_img

    def forward_disc(self, img, y):
        self.model.discriminator.zero_grad()
        gen_img, noise = self.model.generate_imgs(cls=y)
        _, pred_real_img = self.model.discriminator(x=img, cls=y)
        pred_real_img = torch.sigmoid(pred_real_img.reshape(-1))

        label_real_img = torch.ones(pred_real_img.shape[0], device=self.config.device)
        real_img_loss = self.disc_criterion(pred_real_img, label_real_img)
        real_img_loss.backward()

        _, pred_gen_img = self.model.discriminator(x=gen_img.detach(), cls=y)
        pred_gen_img = torch.sigmoid(pred_gen_img.reshape(-1))

        label_gen_img = torch.zeros(pred_gen_img.shape[0], device=self.config.device)
        gen_img_loss = self.disc_criterion(pred_gen_img, label_gen_img)
        gen_img_loss.backward()

        disc_real_acc = pred_real_img.mean().item()
        disc_fake_acc = 1 - pred_gen_img.mean().item()

        disc_loss = gen_img_loss + real_img_loss

        self.disc_optimizer.step()

        return disc_loss, disc_real_acc, disc_fake_acc, gen_img

    def get_logging_dict(self):
        return {"Loss/Disc": None, "Loss/Gen": None, "Gen/DiscFakeAcc": None,
                "Disc/RealAcc": None, "Disc/FakeAcc": None}

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        if config.model_architecture == "biggan":
            model = architecture.BigGAN.from_config(config).to(device=config.device)
        elif config.model_architecture == "dcgan":
            model = architecture.DCGAN.from_config(config).to(device=config.device)
        else:
            raise ValueError()

        gen_criterion = torch.nn.BCELoss()
        disc_criterion = torch.nn.BCELoss()

        gen_optimizer = torch.optim.Adam(model.get_gen_params(), lr=config.lr_gen, betas=config.betas)
        disc_optimizer = torch.optim.Adam(model.get_disc_params(), lr=config.lr_disc, betas=config.betas)

        logger = training_logger.GANLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            model=model,
            gen_criterion=gen_criterion,
            disc_criterion=disc_criterion,
            gen_optimizer=gen_optimizer,
            disc_optimizer=disc_optimizer,
            dataloader=dataloader,
            logger=logger,
            config=config,
        )


class RobGANPipeline(Pipeline):
    def __init__(self, attacker, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.attacker = attacker

    def run_epoch(self, epoch):
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            logging_dict = {}
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            x = x if self.config.upsample_img else self.downsample(x)
            x_adv = self.attacker(x, y, model=self.model.discriminator, criterion=self.disc_criterion)

            if step % self.config.disc_steps == 0:
                self.model.req_grad_disc(False)
                gen_loss, gen_disc_acc, gen_img = self.forward_gen(y)
                logging_dict["gen_loss"] = gen_loss
                logging_dict["gen_disc_acc"] = gen_disc_acc

            if step % self.config.gen_steps == 0:
                self.model.req_grad_disc(True)
                disc_loss, disc_real_acc, disc_fake_acc, real_clf_acc, gen_clf_acc, gen_img = self.forward_disc(y, x_adv)
                logging_dict["disc_loss"] = disc_loss
                logging_dict["disc_real_acc"] = disc_real_acc
                logging_dict["disc_fake_acc"] = disc_fake_acc
                logging_dict["real_clf_acc"] = real_clf_acc
                logging_dict["gen_clf_acc"] = gen_clf_acc

            self.save_img(epoch=epoch, real_img=x, img_gen=gen_img)
            self.save_model(epoch)
            self.logger(epoch, step, logging_dict)

    def forward_gen(self, y):
        self.model.generator.zero_grad()
        y_random = torch.randint_like(y, low=0, high=self.config.num_cls)
        gen_img, noise = self.model.generate_imgs(cls=y_random)
        pred_gen_img, pred_classes = self.model.discriminator(gen_img)

        label_gen_img = torch.ones(pred_gen_img.shape[0], device=self.config.device)
        gen_loss = self.gen_criterion(pred_gen_img, label_gen_img, pred_classes, y_random)
        gen_loss.backward()

        gen_disc_acc = torch.sum(pred_gen_img.data > 0).item() / pred_gen_img.shape[0]
        self.gen_optimizer.step()

        return gen_loss, gen_disc_acc, gen_img

    def forward_disc(self, y, x_adv):
        self.model.discriminator.zero_grad()
        pred_real_img, pred_real_classes = self.model.discriminator(x_adv)

        label_real_img = torch.ones(pred_real_img.shape[0], device=self.config.device)
        real_img_loss = self.disc_criterion(pred_real_img, label_real_img, pred_real_classes, y)
        real_clf_acc = tu.calculate_acc(y, pred_real_classes)
        real_img_loss.backward()

        y_random = torch.randint_like(y, low=0, high=self.config.num_cls)
        gen_img, noise = self.model.generate_imgs(cls=y_random)
        pred_gen_img, pred_gen_classes = self.model.discriminator(gen_img.detach())

        label_gen_img = torch.zeros(pred_gen_img.shape[0], device=self.config.device)
        gen_img_loss = self.disc_criterion(pred_gen_img, label_gen_img, pred_gen_classes, y_random)
        gen_clf_acc = tu.calculate_acc(y_random, pred_gen_classes)
        gen_img_loss.backward()

        disc_real_acc = torch.sum(pred_real_img.data > 0).item() / pred_real_img.shape[0]
        disc_fake_acc = torch.sum(pred_gen_img.data > 0).item() / pred_gen_img.shape[0]

        disc_loss = gen_img_loss + real_img_loss

        self.disc_optimizer.step()

        return disc_loss, disc_real_acc, disc_fake_acc, real_clf_acc, gen_clf_acc, gen_img

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        model = architecture.RobGAN.from_config(config).to(device=config.device)

        gen_criterion = losses.RobGANLoss(config.rob_proportion)
        disc_criterion = losses.RobGANLoss(config.rob_proportion)

        gen_optimizer = torch.optim.Adam(model.get_gen_params(), lr=config.rob_lr, betas=config.betas)
        disc_optimizer = torch.optim.Adam(model.get_disc_params(), lr=config.rob_lr, betas=config.betas)

        attacker = adversarial.AdvAttackerRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        logger = training_logger.GANLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            model=model,
            attacker=attacker,
            gen_criterion=gen_criterion,
            disc_criterion=disc_criterion,
            gen_optimizer=gen_optimizer,
            disc_optimizer=disc_optimizer,
            dataloader=dataloader,
            logger=logger,
            config=config,
        )


class RobBiGANPipeline(Pipeline):
    def __init__(self, attacker, enc_optimizer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.attacker = attacker
        self.enc_optimizer = enc_optimizer

    def run_epoch(self, epoch):
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            logging_dict = {}
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            x = x if self.config.upsample_img else self.downsample(x)
            x_adv = self.attacker(x, y, model=self.model.discriminator, encoder=self.model.encoder, criterion=self.disc_criterion)

            if step % self.config.disc_steps == 0:
                self.model.req_grad_disc(False)
                loss, real_clf_acc, gen_clf_acc, x_fake_adv = self.forward_gen(y, x_adv)
                logging_dict["gen_loss"] = loss

            if step % self.config.gen_steps == 0:
                self.model.req_grad_disc(True)
                loss, real_clf_acc, gen_clf_acc, x_fake_adv = self.forward_disc(y, x_adv)
                logging_dict["disc_loss"] = loss
                logging_dict["real_clf_acc"] = real_clf_acc
                logging_dict["gen_clf_acc"] = gen_clf_acc

            self.save_img(epoch=epoch, real_img=x, img_gen=x_fake_adv)
            self.save_model(epoch)
            self.logger(epoch, step, logging_dict)

    def forward_gen(self, y_real, x_real_adv):
        self.model.generator.zero_grad()
        self.model.encoder.zero_grad()
        y_random = torch.randint_like(y_real, low=0, high=self.config.num_cls)
        x_fake_adv, z_fake_adv = self.model.generate_imgs(cls=y_random)
        z_real_adv = self.model.encoder(x_real_adv)

        pred_generated_true, pred_true_classes = self.model.discriminator(x_real_adv, z_real_adv)
        pred_generated_fake, pred_fake_classes = self.model.discriminator(x_fake_adv, z_fake_adv)

        generated_false = torch.ones(pred_generated_true.shape[0], device=self.config.device)
        generated_true = torch.zeros(pred_generated_fake.shape[0], device=self.config.device)

        loss = self.gen_criterion(
            pred_generated_true, pred_generated_fake, generated_false, generated_true,
            pred_true_classes, pred_fake_classes, y_real)
        loss.backward()

        real_clf_acc = tu.calculate_acc(y_real, pred_true_classes)
        gen_clf_acc = tu.calculate_acc(y_random, pred_fake_classes)
        self.gen_optimizer.step()
        self.enc_optimizer.step()

        return loss, real_clf_acc, gen_clf_acc, x_fake_adv

    def forward_disc(self, y_real, x_real_adv):
        self.model.discriminator.zero_grad()
        y_random = torch.randint_like(y_real, low=0, high=self.config.num_cls)
        x_fake_adv, z_fake_adv = self.model.generate_imgs(cls=y_random)
        z_real_adv = self.model.encoder(x_real_adv)

        pred_generated_true, pred_true_classes = self.model.discriminator(x_real_adv, z_real_adv)
        pred_generated_fake, pred_fake_classes = self.model.discriminator(x_fake_adv, z_fake_adv)

        generated_false = torch.ones(pred_generated_true.shape[0], device=self.config.device)
        generated_true = torch.zeros(pred_generated_fake.shape[0], device=self.config.device)

        loss = self.gen_criterion(
            pred_generated_true, pred_generated_fake, generated_true, generated_false,
            pred_true_classes, pred_fake_classes, y_real)
        loss.backward()

        real_clf_acc = tu.calculate_acc(y_real, pred_true_classes)
        gen_clf_acc = tu.calculate_acc(y_random, pred_fake_classes)
        self.disc_optimizer.step()

        return loss, real_clf_acc, gen_clf_acc, x_fake_adv

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        model = architecture.RobBiGAN.from_config(config).to(device=config.device)

        gen_criterion = losses.RobBiGANLoss(config.rob_proportion)
        disc_criterion = losses.RobBiGANLoss(config.rob_proportion)

        gen_optimizer = torch.optim.Adam(model.get_gen_params(), lr=config.rob_lr, betas=config.betas)
        enc_optimizer = torch.optim.Adam(model.get_enc_params(), lr=config.rob_lr, betas=config.betas)
        disc_optimizer = torch.optim.Adam(model.get_disc_params(), lr=config.rob_lr, betas=config.betas)

        attacker = adversarial.AdvAttackerBiRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        logger = training_logger.GANLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            model=model,
            attacker=attacker,
            gen_criterion=gen_criterion,
            disc_criterion=disc_criterion,
            gen_optimizer=gen_optimizer,
            enc_optimizer=enc_optimizer,
            disc_optimizer=disc_optimizer,
            dataloader=dataloader,
            logger=logger,
            config=config,
        )


class RobBigBiGANPipeline(AbsBigBiGANPipeline):
    def __init__(self, attacker, baseline_noattack, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.baseline_noattack = baseline_noattack
        self.attacker = attacker

    def run_epoch(self, epoch):
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            x_adv = x if self.baseline_noattack else self.attacker(x, y, model=self.model, criterion=self.disc_criterion)

            logging_dict = self.get_logging_dict()
            self.model.req_grad_disc(True)
            if step % self.config.gen_steps == 0:
                y_random = torch.randint_like(y, low=0, high=self.config.num_cls)
                img_gen, noise = self.model.generate_imgs(cls=y_random)
                z_img = self.model.generate_latent(img=x_adv)
                self.disc_optimizer.zero_grad()
                outputs = self.model.forward(
                    img_real=x_adv,
                    img_gen=img_gen.detach(),
                    z_noise=noise,
                    z_img=z_img.detach(),
                )
                disc_loss = self.disc_criterion.forward_disc(outputs, y, y_random)
                logging_dict["disc_clf_acc"] = tu.calculate_acc(y, outputs["pred_real_classes"])
                logging_dict["gen_clf_acc"] = tu.calculate_acc(y_random, outputs["pred_gen_classes"])
                disc_loss.backward()
                self.disc_optimizer.step()
                self.log_statistics(disc_loss, logging_dict, outputs, "postdisc")

            if step % self.config.disc_steps == 0:
                self.model.req_grad_disc(False)
                self.gen_optimizer.zero_grad()
                self.enc_optimizer.zero_grad()
                y_random = torch.randint_like(y, low=0, high=self.config.num_cls)
                img_gen, noise = self.model.generate_imgs(cls=y_random)
                z_img = self.model.generate_latent(img=x_adv)
                outputs = self.model.forward(img_real=x_adv, img_gen=img_gen, z_noise=noise, z_img=z_img)
                gen_enc_loss = self.gen_criterion.forward_gen(outputs, y, y_random)
                gen_enc_loss.backward()
                self.gen_optimizer.step()
                self.enc_optimizer.step()
                self.log_statistics(gen_enc_loss, logging_dict, outputs, "postgen")

            self.save_img(epoch=epoch, real_img=x, img_gen=img_gen, latent=z_img, y=y)
            self.save_model(epoch)
            self.logger(epoch=epoch, step=step, logging_dict=logging_dict)

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        model = architecture.RobBigBiGAN.from_config(config).to(device=config.device)

        gen_enc_criterion = losses.RobBigBiGANLoss(config.rob_proportion)
        disc_criterion = losses.RobBigBiGANLoss(config.rob_proportion)

        gen_optimizer = torch.optim.Adam(model.get_gen_params(), lr=config.lr_gen, betas=config.betas)
        enc_optimizer = torch.optim.Adam(model.get_enc_params(), lr=config.lr_enc, betas=config.betas)
        disc_optimizer = torch.optim.Adam(model.get_disc_params(), lr=config.lr_disc, betas=config.betas)

        attacker = adversarial.AdvAttackerBigBiRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        logger = training_logger.BiGANLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            model=model,
            attacker=attacker,
            baseline_noattack=config.baseline_noattack,
            gen_criterion=gen_enc_criterion,
            disc_criterion=disc_criterion,
            gen_optimizer=gen_optimizer,
            enc_optimizer=enc_optimizer,
            disc_optimizer=disc_optimizer,
            dataloader=dataloader,
            logger=logger,
            config=config,
        )


class RobBigBiGANInference:
    def __init__(self, baseline_noattack, attacker, model, dataloader, disc_criterion, config):
        self.model = model
        self.dataloader = dataloader
        self.config = config
        self.baseline_noattack = baseline_noattack
        self.attacker = attacker
        self.disc_criterion = disc_criterion

    def inference(self):
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            org_img = x
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            x_adv = x if self.baseline_noattack else self.attacker(x, y, model=self.model, criterion=self.disc_criterion)
            org_adv_img = x_adv.detach()
            z_noattack_img = self.model.generate_latent(img=x)
            z_img = self.model.generate_latent(img=x_adv)
            img_gen, noise = self.model.generate_imgs(cls=y)
            rec_img_gen, rec_noise = self.model.generate_imgs(cls=y, noise=z_img)

            rec_noattack_img_gen, _ = self.model.generate_imgs(cls=y, noise=z_noattack_img)

            imgs = [("org_img", org_img), ("org_adv_img", org_adv_img),
                    ("gen_img", img_gen), ("reconstructed_img", rec_img_gen),
                    ("rec_noattack_img_gen", rec_noattack_img_gen)]
            self.save_img_grid(imgs)
            break

    def generate_img_dataset(self, name):
        imgs = []
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            img_gen, noise = self.model.generate_imgs(cls=y)

            imgs.append(img_gen.detach().cpu().numpy())

        imgs = np.vstack(imgs)
        file_name = f"{name}.npz"
        gen_imgs_save_folder = Path(self.config.rec_imgs_save_path.format(
            ds_name=self.config.ds_name,
            model_architecture=self.config.model_architecture,
            hparams="generated_image_dataset",
        ))
        gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
        gen_imgs_save_path = str(gen_imgs_save_folder / file_name)

        with open(gen_imgs_save_path, 'wb') as f:
            np.save(f, imgs)

    def generate_lat_dataset(self, name):
        imgs = []
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            z_img = self.model.generate_latent(img=x)
            rec_img_gen, _ = self.model.generate_imgs(cls=y, noise=z_img)

            imgs.append(rec_img_gen.detach().cpu().numpy())

        imgs = np.vstack(imgs)
        file_name = f"{name}.npz"
        gen_imgs_save_folder = Path(self.config.rec_imgs_save_path.format(
            ds_name=self.config.ds_name,
            model_architecture=self.config.model_architecture,
            hparams="generated_image_dataset",
        ))
        gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
        gen_imgs_save_path = str(gen_imgs_save_folder / file_name)

        with open(gen_imgs_save_path, 'wb') as f:
            np.save(f, imgs)

    def encode(self, img):
        z_img = self.model.generate_latent(img=img)
        return z_img

    def generate(self, y, latent):
        img_gen, noise = self.model.generate_imgs(cls=y, noise=latent)  # generated imgs
        return img_gen, noise

    def save_img_grid(self, imgs):
        for name, img in imgs:
            img = img.detach().cpu()[:self.config.save_img_count, ...]
            img = np.transpose(vutils.make_grid(
                img, padding=2, nrow=self.config.img_rows, normalize=True), (1, 2, 0))
            plt.imshow(img)

            file_name = f"{name}.png"
            gen_imgs_save_folder = Path(self.config.rec_imgs_save_path.format(
                ds_name=self.config.ds_name,
                model_architecture=self.config.model_architecture,
                hparams=self.config.hparams_str,
            ))
            gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
            gen_imgs_save_path = str(gen_imgs_save_folder / file_name)
            plt.savefig(fname=gen_imgs_save_path)

    @classmethod
    def from_checkpoint(cls, data_path, checkpoint_path, config):
        config.device = torch.device(config.device)
        dataloader_fn = data_loading.get_supported_loader(config.ds_name)
        dataloader = dataloader_fn(data_path=data_path, config=config, ds_type="train")
        model = architecture.RobBigBiGAN.from_config(config).to(device=config.device)
        checkpoint = torch.load(checkpoint_path)
        model.load_state_dict(checkpoint, strict=True)
        model = model.cuda()
        # model = model.eval()

        disc_criterion = losses.RobBigBiGANLoss(config.rob_proportion)

        attacker = adversarial.AdvAttackerBigBiRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        return cls(
            model=model,
            attacker=attacker,
            baseline_noattack=config.baseline_noattack,
            disc_criterion=disc_criterion,
            dataloader=dataloader,
            config=config,
        )