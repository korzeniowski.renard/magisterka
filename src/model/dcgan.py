from torch import nn


in_channels = 3
kernel_size = 4

disc_channels_shapes = [in_channels, 128, 256, 512, 1]
spectral_norm = False
disc_strides = [2, 2, 2, 1]
disc_paddings = [1, 1, 1, 0]
disc_bn = [False] + [True] * (len(disc_strides) - 2) + [False]

latent_shape = 100
gen_channels_shapes = [latent_shape, 512, 256, 128, in_channels]
gen_strides = [1, 2, 2, 2]
gen_bn = [True] * (len(gen_strides) - 1) + [False]
gen_paddings = [0, 1, 1, 1]


def weights_init(model):
    classname = model.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(model.weight.data, 0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(model.weight.data, 1, 0.02)
        nn.init.normal_(model.bias.data, 0)


class BasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, act, frac_conv=False, bn=True, spectral_norm=False):
        super().__init__()
        conv_cls = nn.ConvTranspose2d if frac_conv else nn.Conv2d
        self.conv = conv_cls(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            bias=False,
        )
        if spectral_norm:
            self.conv = nn.utils.spectral_norm(self.conv)

        self.bn = self.get_batch_norm(bn, out_channels)
        self.act = act

    def get_batch_norm(self, batch_norm, out_channels):
        if batch_norm:
            bn = nn.BatchNorm2d(num_features=out_channels)
        else:
            bn = None
        return bn

    def forward(self, X):
        out = self.conv(X)
        if self.bn: out = self.bn(out)
        out = self.act(out)
        return out


class Discriminator(nn.Module):
    def __init__(self, disc_channels_shapes=disc_channels_shapes, spectral_norm=spectral_norm, disc_bn=disc_bn, kernel_size=kernel_size,
                 strides=disc_strides, paddings=disc_paddings):
        super().__init__()
        ins, outs = disc_channels_shapes[:-1], disc_channels_shapes[1:]
        acts = (len(ins)-1) * [nn.LeakyReLU(negative_slope=0.2)] + [nn.Sigmoid()]

        if disc_bn:
            batch_norms = [False] + [True] * (len(strides) - 2) + [False]
        else:
            batch_norms = [False] * len(strides)

        self.layers = nn.ModuleList([
            BasicBlock(input_shape, output_shape,
                kernel_size=kernel_size, stride=stride, padding=padding, act=act, bn=bn, spectral_norm=spectral_norm)
            for input_shape, output_shape, stride, padding, act, bn
            in zip(ins, outs, strides, paddings, acts, batch_norms)
        ])

    def forward(self, x, cls=None):
        dummy_score = None
        for layer in self.layers:
            x = layer.forward(x)
        return dummy_score, x


class Generator(nn.Module):
    def __init__(self, gen_channels_shapes=gen_channels_shapes, kernel_size=kernel_size,
                 strides=gen_strides, paddings=gen_paddings,
                 batch_norms=gen_bn):
        super().__init__()
        ins, outs = gen_channels_shapes[:-1], gen_channels_shapes[1:]
        acts = (len(ins)-1) * [nn.ReLU()] + [nn.Tanh()]

        self.layers = nn.ModuleList([
            BasicBlock(input_shape, output_shape, frac_conv=True,
                       kernel_size=kernel_size, stride=stride, padding=padding, act=act, bn=bn)
            for input_shape, output_shape, stride, padding, act, bn in
            zip(ins, outs, strides, paddings, acts, batch_norms)
        ])

    def forward(self, z):
        x = z.float()
        for layer in self.layers:
            x = layer.forward(x)
        return x
