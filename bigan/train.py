import torch
import torch.nn as nn
from torch import optim
from torch.autograd import Variable
import torchvision.utils as vutils

from barbar import Bar

from model import BiGAN


CHECKPOINT_PATH = './default_checkpoint_BiGAN_epoch_{}.pt'


class TrainerBiGAN:
    def __init__(self, args, data, device):
        self.args = args
        self.train_loader = data
        self.device = device
        self.model = BiGAN(self.args, self.device)

    def train(self):
        """Training the BiGAN"""

        if self.args.wasserstein:
            optimizer_ge = optim.RMSprop(list(self.model.G.parameters()) +
                                         list(self.model.E.parameters()), lr=self.args.lr_rmsprop)
            optimizer_d = optim.RMSprop(self.model.D.parameters(), lr=self.args.lr_rmsprop)
        else:
            optimizer_ge = optim.Adam(list(self.model.G.parameters()) +
                                      list(self.model.E.parameters()), lr=self.args.lr_adam)
            optimizer_d = optim.Adam(self.model.D.parameters(), lr=self.args.lr_adam)

        fixed_z = Variable(torch.randn((16, self.args.latent_dim, 1, 1)),
                           requires_grad=False).to(self.device)
        criterion = nn.BCELoss()

        for epoch in range(self.args.start_epoch, self.args.num_epochs+1):
            ge_losses = 0
            d_losses = 0
            for x, _ in Bar(self.train_loader):
                #Defining labels
                y_true = Variable(torch.ones((x.size(0), 1)).to(self.device))
                y_fake = Variable(torch.zeros((x.size(0), 1)).to(self.device))

                #Noise for improving training.
                noise1 = Variable(torch.Tensor(x.size()).normal_(0, 
                                  0.1 * (self.args.num_epochs - epoch + 1) / (self.args.num_epochs + 1)),
                                  requires_grad=False).to(self.device)
                noise2 = Variable(torch.Tensor(x.size()).normal_(0, 
                                  0.1 * (self.args.num_epochs - epoch + 1) / (self.args.num_epochs + 1)),
                                  requires_grad=False).to(self.device)

                # Cleaning gradients.
                optimizer_d.zero_grad()

                # Generator:
                z_fake = Variable(torch.randn((x.size(0), self.args.latent_dim, 1, 1)).to(self.device),
                                  requires_grad=False)
                x_fake = self.model.G(z_fake)

                # Encoder:
                x_true = x.float().to(self.device)
                z_true = self.model.E(x_true)

                # Discriminator
                out_true = self.model.D(x_true + noise1, z_true)
                out_fake = self.model.D(x_fake + noise2, z_fake)

                # Losses
                if self.args.wasserstein:
                    loss_d = - torch.mean(out_true) + torch.mean(out_fake)
                else:
                    loss_d = criterion(out_true, y_true) + criterion(out_fake, y_fake)

                # Computing gradients and backpropagate.
                loss_d.backward()
                optimizer_d.step()

                # Cleaning gradients.
                optimizer_ge.zero_grad()

                # Generator:
                z_fake = Variable(torch.randn((x.size(0), self.args.latent_dim, 1, 1)).to(self.device),
                                  requires_grad=False)
                x_fake = self.model.G(z_fake)

                # Encoder:
                x_true = x.float().to(self.device)
                z_true = self.model.E(x_true)

                # Discriminator
                out_true = self.model.D(x_true + noise1, z_true)
                out_fake = self.model.D(x_fake + noise2, z_fake)

                # Losses
                if self.args.wasserstein:
                    loss_ge = - torch.mean(out_fake) + torch.mean(out_true)
                else:
                    loss_ge = criterion(out_fake, y_true) + criterion(out_true, y_fake)

                loss_ge.backward()
                optimizer_ge.step()

                if self.args.wasserstein:
                    for p in self.model.D.parameters():
                        p.data.clamp_(-self.args.clamp, self.args.clamp)
                
                ge_losses += loss_ge.item()
                d_losses += loss_d.item()

            if epoch % 50 == 0:
                vutils.save_image(self.model.G(fixed_z).data, './{}_fake.png'.format(epoch))
                torch.save(self.model.state_dict(), CHECKPOINT_PATH.format(epoch))

            print("Training... Epoch: {}, Discrimiantor Loss: {:.3f}, Generator Loss: {:.3f}".format(
                epoch, d_losses/len(self.train_loader), ge_losses/len(self.train_loader)
            ))
