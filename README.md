# Magisterka

# TODO BigBiGAN
(bad loss direction gen loss -> inf disc_loss -> -inf)
- [x] emb dim should be independent from number of layers 
- [x] check if shapes agree (especially mpl discriminators)
- [x] check if attention is ok
- [x] check initialization
- [x] loss is ok (still seems like major suspect)
- [x] weight init (tf.initializers.orthogonal() if mnist tf.initializers.TruncatedNormal(mean=0.0, stddev=0.02))
- [x] latent shape per layer
- [x] spectral norm the same for tf and torch?
- [x] weight_regularizer in resblock_up_condition blocks, condition_batch_norm with weight_regularizer_fully, dense and conv2dtrans in gen
- [x] bias in correct layers
- [x] top layer with different ratio of channels in block
- [x] w_init=w_init in cond bn
- [x] non conditional support
- [x] disc + gen check if works as gan
- [x] enc check if works for classification
- [x] check enc double channel fn

- [x] sig on all disciminators in bigan (both losses go to 0)
- [ ] leaky relu in disc
- [ ] why loss value is so different in tf (disc ~1/ gen ~10) vs torch(disc ~ -1000/ gen 1000)
