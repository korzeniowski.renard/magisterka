import torch
import torch.nn as nn
import torch.nn.functional as F

from src.model import layers
from src.model import rob_layers
from src.training_utils import training_utils


class GenBigGAN(nn.Module):
    def __init__(self, mult_chs, ks, num_cls, latent_dim, embedding_dim, sn, w_init):
        super().__init__()
        self.ch = mult_chs["pre"][0]
        self.conditional = num_cls > 0

        m_pre_chs, m_post_chs, out_ch = mult_chs["pre"], mult_chs["post"], mult_chs["colors"]
        self.splits = (len(m_pre_chs) + len(m_post_chs) + 1)
        split_latent_dim = latent_dim // self.splits
        assert latent_dim % self.splits == 0, "latent has to be divisible by number of CondResnetBlocks layers"

        m_post_chs = training_utils.get_channel_inputs(m_post_chs, input_dim=m_pre_chs[-1])
        m_pre_chs = training_utils.get_channel_inputs(m_pre_chs, input_dim=m_pre_chs[0])
        top_block = [True] + [False] * (len(m_pre_chs) - 1)

        cond_dim = split_latent_dim + embedding_dim if self.conditional else split_latent_dim

        if self.conditional:
            self.class_embedding = nn.Embedding(num_embeddings=num_cls, embedding_dim=embedding_dim)
        self.linear = layers.LinearSN(in_features=split_latent_dim, out_features=4 * 4 * self.ch, sn=sn, w_init=w_init)
        # tf 4 * 4 * 256 # here 4 * 4 * 256
        self.pre_up_blocks = nn.Sequential(*[
            layers.UpResnetBlock(in_m, out_m, ks, cond_dim, sn, bias=False, w_init=w_init, first=f)
            for (in_m, out_m), f in zip(m_pre_chs, top_block)
        ]) # tf 256 -> 128 # here 256, 128
        self.non_loc = layers.SelfAttn(mult_chs["pre"][-1], sn=sn) # tf 128 -> # here 128
        # should be 2 times bigger same as output of prev block i.e. 256 // 2
        # but this implementation keeps the same dim so  ch // 2 -> attn -> ch // 4
        self.post_up_blocks = nn.Sequential(*[
            layers.UpResnetBlock(in_m, out_m, ks, cond_dim, sn, bias=False, w_init=w_init)
            for in_m, out_m in m_post_chs
        ]) # tf -> 64 # 64

        self.bn = nn.BatchNorm2d(mult_chs["post"][-1])
        self.relu = nn.ReLU()
        self.conv = layers.ConvTranspose2dSN(
            in_channels=mult_chs["post"][-1], out_channels=out_ch,
            kernel_size=ks, padding=1, sn=sn, bias=False, w_init=w_init)
        self.sigmoid = nn.Sigmoid()

    def forward(self, z, cls, scale=True):
        z = z.float()
        all_z = z.chunk(self.splits, dim=-1)
        z, conds = all_z[0], all_z[1:]

        if self.conditional:
            cls_embed = self.class_embedding(cls)
            conds = [torch.cat([conds[d], cls_embed], dim=-1) for d in range(len(conds))]

        z = self.linear(z)
        z = z.reshape(-1, self.ch, 4, 4)

        for i, layer in enumerate(self.pre_up_blocks):
            z = layer(z, cond=conds[i])

        z = self.non_loc(z)

        for i, layer in enumerate(self.post_up_blocks, start=len(self.pre_up_blocks)):
            z = layer(z, cond=conds[i])

        z = self.bn(z)
        z = self.relu(z)
        x = self.conv(z)
        x = self.sigmoid(x) if scale else x
        return x

    @classmethod
    def from_config(cls, config):
        return cls(
            mult_chs=config.gen_mult_chs,
            ks=config.ks,
            num_cls=config.num_cls,
            latent_dim=config.latent_dim,
            embedding_dim=config.embedding_dim,
            w_init=config.w_init,
            sn=config.spectral_norm,
        )


class BiGANGenerator(nn.Module):
    def __init__(self, z_dim=32):
        super(BiGANGenerator, self).__init__()
        self.z_dim = z_dim

        self.output_bias = nn.Parameter(torch.zeros(3, 32, 32), requires_grad=True)
        self.deconv1 = nn.ConvTranspose2d(z_dim, 256, 4, stride=1, bias=False)
        self.bn1 = nn.BatchNorm2d(256)
        self.deconv2 = nn.ConvTranspose2d(256, 128, 4, stride=2, bias=False)
        self.bn2 = nn.BatchNorm2d(128)
        self.deconv3 = nn.ConvTranspose2d(128, 64, 4, stride=1, bias=False)
        self.bn3 = nn.BatchNorm2d(64)
        self.deconv4 = nn.ConvTranspose2d(64, 32, 4, stride=2, bias=False)
        self.bn4 = nn.BatchNorm2d(32)
        self.deconv5 = nn.ConvTranspose2d(32, 32, 5, stride=1, bias=False)
        self.bn5 = nn.BatchNorm2d(32)
        self.deconv6 = nn.Conv2d(32, 3, 1, stride=1, bias=True)

    def forward(self, z):
        z = F.leaky_relu(self.bn1(self.deconv1(z)), negative_slope=0.1)
        z = F.leaky_relu(self.bn2(self.deconv2(z)), negative_slope=0.1)
        z = F.leaky_relu(self.bn3(self.deconv3(z)), negative_slope=0.1)
        z = F.leaky_relu(self.bn4(self.deconv4(z)), negative_slope=0.1)
        z = F.leaky_relu(self.bn5(self.deconv5(z)), negative_slope=0.1)
        return torch.sigmoid(self.deconv6(z) + self.output_bias)


class ResNetRob(nn.Module):
    def __init__(self, ch, dim_z, bottom_width, n_classes, activation=F.relu, distribution="normal"):
        super().__init__()
        self.bottom_width = bottom_width
        self.activation = activation
        self.distribution = distribution
        self.dim_z = dim_z
        self.n_classes = n_classes
        self.l1 = nn.Linear(dim_z, (bottom_width ** 2) * ch * 16)
        nn.init.xavier_uniform_(self.l1.weight, 1.0)
        self.block2 = rob_layers.UpsampleBlock(ch * 16, ch * 8, activation=activation, upsample=True, n_classes=n_classes)
        self.block3 = rob_layers.UpsampleBlock(ch * 8, ch * 4, activation=activation, upsample=True, n_classes=n_classes)
        self.block4 = rob_layers.UpsampleBlock(ch * 4, ch * 2, activation=activation, upsample=True, n_classes=n_classes)
        self.block5 = rob_layers.UpsampleBlock(ch * 2, ch * 1, activation=activation, upsample=False, n_classes=n_classes)
        self.b6 = nn.BatchNorm2d(ch)
        nn.init.constant_(self.b6.weight, 1.0)
        self.l6 = nn.Conv2d(ch, 3, kernel_size=3, stride=1, padding=1)
        nn.init.xavier_uniform_(self.l6.weight, 1.0)

    def forward(self, z, y):
        h = z
        h = self.l1(h)
        h = h.view(h.size(0), -1, self.bottom_width, self.bottom_width)
        h = self.block2(h, y)
        h = self.block3(h, y)
        h = self.block4(h, y)
        h = self.block5(h, y)
        h = self.b6(h)
        h = self.activation(h)
        h = self.l6(h)
        h = torch.tanh(h)
        return h

