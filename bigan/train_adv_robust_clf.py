
import matplotlib.pyplot as plt
import argparse
import torch
from train import TrainerBiGAN
from preprocess import get_cifar10
from barbar import Bar

CHECKPOINT_PATH = './robust_checkpoint_clf_on_BiGAN_reps_epoch_{}.pt'

train_eps = 0.1
epochs = 100
class_count = 10
representation_dim = 256
eps_list = [0.0, 0.01, 0.05, 0.1, 0.2, 0.5]
attack_type = 'inf'

parser = argparse.ArgumentParser()
parser.add_argument("--num_epochs", type=int, default=200,
                    help="number of epochs")
parser.add_argument('--lr_adam', type=float, default=1e-4,
                    help='learning rate')
parser.add_argument('--lr_rmsprop', type=float, default=1e-4,
                    help='learning rate RMSprop if WGAN is True.')
parser.add_argument("--batch_size", type=int, default=128,
                    help="Batch size")
parser.add_argument('--latent_dim', type=int, default=256,
                    help='Dimension of the latent variable z')
parser.add_argument('--wasserstein', type=bool, default=False,
                    help='If WGAN.')
parser.add_argument('--clamp', type=float, default=1e-2,
                    help='Clipping gradients for WGAN.')
parser.add_argument('--start_epoch', type=int, default=0,
                    help='Start epoch based on checkpoint.')
parser.add_argument('--checkpoint', type=str, default=None,
                    help='Path to checkpoint.')
parser.add_argument('--head_checkpoint', type=str, default=None,
                    help='Path to checkpoint.')

#parsing arguments.
args = parser.parse_args()


class RepresentationClassifier(torch.nn.Module):
    def __init__(self, head, encoder):
        super().__init__()
        self.head = head
        self.encoder = encoder

    def forward(self, x, with_enc_grads=False):
        if with_enc_grads:
            enc_x = encoder(x).squeeze()
        else:
            with torch.no_grad():
                enc_x = encoder(x).squeeze()

        outputs = net(enc_x)
        return outputs


def train_robust_model(epochs, data, model, optimiser, eps, attack_type='inf'):
    for epoch in range(epochs):
        print(f"ep {epoch + 1}/{50}")
        running_acc = 0
        best_acc = 0
        for i, (x, y) in enumerate(Bar(data)):
            x = x.to(device)
            x_adv = projected_gradient_descent(model, x, y, criterion,
                                               num_steps=40, step_size=0.01,
                                               eps=eps, eps_norm=attack_type,
                                               step_norm=attack_type)
            optimiser.zero_grad()
            y_pred = model(x_adv).cpu()
            loss = criterion(y_pred, y)
            loss.backward()
            optimiser.step()

            running_acc += calculate_acc(preds=y_pred, labels=y)
            if i % 50 == 0:
                print(f"epoch {epoch + 1}, batch {i + 1}, loss {loss}")
                print(f"accuracy {running_acc / (i + 1)}")
        if epoch % 50:
            torch.save(net.state_dict(), CHECKPOINT_PATH.format(epoch))
        best_acc = max(best_acc, running_acc)
        print(f"best_acc {best_acc / (i + 1)}")


def calculate_acc(preds, labels):
    preds = preds.max(dim=1)[1].numpy()
    labels = labels.numpy()
    return sum(preds == labels) / len(labels)


def perform_attack(data, model, criterion, device, eps=0.3, attack_type='inf'):
    running_acc = 0
    for i, (x, y) in enumerate(Bar(data)):
        x = x.to(device)
        x_adv = projected_gradient_descent(model, x, y, criterion,
                                           num_steps=40, step_size=0.01,
                                           eps=eps, eps_norm=attack_type,
                                           step_norm=attack_type)
        y_pred = model(x_adv).cpu()
        running_acc += calculate_acc(preds=y_pred, labels=y)
    acc = running_acc / (i + 1)
    print(f"acc {acc}")
    return acc


def evaluate_attack_effectiveness(model, data, eps_list, name, attack_type, device, criterion):
    accs = []
    for eps in eps_list:
        print(f"start adv attack for eps {eps}")
        acc = perform_attack(data=data, model=model, eps=eps, attack_type=attack_type, device=device, criterion=criterion)
        accs.append(acc)
        print(f"adv attack for eps {eps} final acc {acc}")

    plt.plot(eps_list, accs, label=name)


def projected_gradient_descent(model, x, y, loss_fn, num_steps, step_size, step_norm, eps, eps_norm,
                               clamp=(0, 1)):
    """Performs the projected gradient descent attack on a batch of images."""
    x_adv = x.clone().detach().requires_grad_(True).to(x.device)
    num_channels = x.shape[1]

    for i in range(num_steps):
        _x_adv = x_adv.clone().detach().requires_grad_(True)

        prediction = model(_x_adv, with_enc_grads=True)

        y = y.to(device)
        loss_fn = loss_fn.to(device)
        loss = loss_fn(prediction, y)
        loss.backward()

        with torch.no_grad():
            # Force the gradient step to be a fixed size in a certain norm
            if step_norm == 'inf':
                gradients = _x_adv.grad.sign() * step_size
            else:
                # Note .view() assumes batched image data as 4D tensor
                gradients = _x_adv.grad * step_size / _x_adv.grad.view(_x_adv.shape[0], -1) \
                    .norm(step_norm, dim=-1) \
                    .view(-1, num_channels, 1, 1)

            # Untargeted: Gradient ascent on the loss of the correct label w.r.t.
            # the model parameters
            x_adv += gradients

        # Project back into l_norm ball and correct range
        if eps_norm == 'inf':
            # Workaround as PyTorch doesn't have elementwise clip
            x_adv = torch.max(torch.min(x_adv, x + eps), x - eps)
        else:
            delta = x_adv - x

            # Assume x and x_adv are batched tensors where the first dimension is
            # a batch dimension
            mask = delta.view(delta.shape[0], -1).norm(step_norm, dim=1) <= eps

            scaling_factor = delta.view(delta.shape[0], -1).norm(step_norm, dim=1)
            scaling_factor[mask] = eps

            # .view() assumes batched images as a 4D Tensor
            delta *= eps / scaling_factor.view(-1, 1, 1, 1)

            x_adv = x + delta

        x_adv = x_adv.clamp(*clamp)

    return x_adv.detach()


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
net = torch.nn.Sequential(
    torch.nn.Linear(representation_dim, representation_dim // 2),
    torch.nn.ReLU(),
    torch.nn.Linear(representation_dim // 2, class_count),
)
criterion = torch.nn.CrossEntropyLoss(reduce='sum')

data = get_cifar10(args)
bigan = TrainerBiGAN(args, data, device)
bigan.model.load_state_dict(torch.load(args.checkpoint, map_location=torch.device(device)))
bigan.model.eval()
encoder = bigan.model.E

model = RepresentationClassifier(head=net, encoder=encoder).to(device)
optimizer = torch.optim.SGD(model.head.parameters(), lr=0.001, momentum=0.9)

train_robust_model(epochs=epochs, data=data, model=model,
                   optimiser=optimizer, attack_type=attack_type, eps=train_eps)

evaluate_attack_effectiveness(
    model=model, data=data, eps_list=eps_list, attack_type=attack_type, name="normal", device=device, criterion=criterion)
plt.show()
