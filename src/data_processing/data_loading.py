from torchvision.datasets import FashionMNIST, MNIST
from torchvision import datasets, transforms
from torch.utils import data as tdataset
import torch

from src.data_processing import datasets as mydatasets


class Databunch:
    def __init__(self, datasets):
        self.datasets = datasets

    def get_datasets_sizes(self):
        return {name: len(dl) for name, dl in self.datasets.items()}

    def __getitem__(self, key):
        return self.datasets[key]

    @classmethod
    def from_config(cls, config, data_path):
        train_dl = get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="train")
        test_dl = get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="test")
        return cls(datasets={"train": train_dl, "test": test_dl})


def get_dataloader(dataset, ds_type, bs):
    loader = tdataset.DataLoader(
        dataset,
        batch_size=bs,
        shuffle=True if ds_type == "train" else False,
        drop_last=True,
    )
    return loader


def get_transforms(config):
    if config.augment_data == "basic":
        trans = transforms.Compose([
            transforms.Resize(config.image_size),
            transforms.CenterCrop(config.image_size),
            transforms.RandomHorizontalFlip(p=config.horizontal_flip_p),
            transforms.ToTensor(),
        ])
    elif config.augment_data == "advanced":
        def noise(x):
            return x + torch.rand_like(x) * (1.0 / 128)
        trans = transforms.Compose([
            transforms.RandomCrop(config.image_size, padding=4),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            # transforms.Normalize(mean=[.5, .5, .5], std=[.5, .5, .5]),
            transforms.Lambda(noise)])
    else:
        trans = transforms.Compose([
            transforms.Resize(config.image_size),
            transforms.CenterCrop(config.image_size),
            transforms.ToTensor(),
        ])
    return trans


def get_CIFAR10_loader(data_path, ds_type, config):
    dataset = datasets.CIFAR10(
        root=data_path,
        train=True if ds_type == "train" else False,
        download=True,
        transform=get_transforms(config),
    )
    return get_dataloader(dataset, ds_type, config.bs)


def get_CIFAR100_loader(data_path, ds_type, config):
    dataset = datasets.CIFAR100(
        root=data_path,
        train=True if ds_type == "train" else False,
        download=True,
        transform=get_transforms(config),
    )
    return get_dataloader(dataset, ds_type, config.bs)


def get_imagenette_loader(data_path, ds_type, config): # shorter edge 160
    dataset = mydatasets.Imagenette(
        root=data_path,
        train=True if ds_type == "train" else False,
        transform=get_transforms(config),
    )
    return get_dataloader(dataset, ds_type, config.bs)


def get_imagewoof_loader(data_path, ds_type, config): # shorter edge 160
    dataset = mydatasets.Imagenette(
        root=data_path,
        train=True if ds_type == "train" else False,
        csv="noisy_imagewoof.csv",
        transform=get_transforms(config),
    )
    return get_dataloader(dataset, ds_type, config.bs)


def get_FMNIST_loader(data_path, ds_type, config):
    dataset = FashionMNIST(
        data_path,
        train=True if ds_type == "train" else False,
        download=True,
        transform=transforms.Compose([
            transforms.Resize(config.image_size),
            transforms.ToTensor(),
        ])
    )
    return get_dataloader(dataset, ds_type, config.bs)


def get_MNIST_loader(data_path, ds_type, config):
    dataset = MNIST(
        data_path,
        train=True if ds_type == "train" else False,
        download=True,
        transform=transforms.Compose([
            transforms.Resize(config.image_size),
            transforms.ToTensor(),
        ])
    )
    return get_dataloader(dataset, ds_type, config.bs)


loaders = {
    "MNIST": get_MNIST_loader,
    "FMNIST": get_FMNIST_loader,
    "CIFAR10": get_CIFAR10_loader,
    "CIFAR100": get_CIFAR100_loader,
    "imagenette": get_imagenette_loader,
    "imagewoof": get_imagewoof_loader,
}


def get_supported_loader(name):
    return loaders[name]
