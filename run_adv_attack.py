import argparse

from src.pipeline import adversarial_pipeline
from src.training_utils import training_utils

parser = argparse.ArgumentParser()
parser.add_argument("--dataset", type=str, default="FMNIST",
                    choices=["FMNIST", "MNIST", "CIFAR10", "CIFAR100", "imagenette", "imagewoof"], help="dataset name")
parser.add_argument("--data_path", type=str, default="../input/fmnist-dataset",
                    help="path to dataset root folder")
parser.add_argument("--model_architecture", type=str, default="bigbigan",
                    choices=["bigbiwgan", "bigbigan", "biggan", "classifier"], help="type of architecture used in training")
args = parser.parse_args()


def run_experiments():
    config = training_utils.get_config(args.dataset)
    hparams_str = ""
    config["model_architecture"] = args.model_architecture
    config["hparams_str"] = hparams_str.strip("_")
    config["seed"] = config.seed
    config["adv_attacks_imgs_save_path"] = "./data/{ds_name}_disc/{model_architecture}/adv_attack/{hparams}"
    run_experiment(config)


def run_experiment(config):
    training_utils.set_random_seed(seed=config.seed, device=config.device)
    training_pipeline = adversarial_pipeline.PGDAdversarialAttackPipeline.from_checkpoint(
        data_path=args.data_path,
        config=config
    )
    training_pipeline.run_experiment()


run_experiments()
