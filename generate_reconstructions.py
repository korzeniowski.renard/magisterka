import argparse

from src.pipeline import pipeline
from src.training_utils import training_utils

checkpoint_path = "/home/ec2-user/renard/bigbigan/data/CIFAR10/robbigbigan/checkpoints/bs-128_save_name-normal_augment_data-basic_baseline_noattack-True/checkpoint_300.pth"
# "/home/ec2-user/renard/bigbigan/data/CIFAR10/robbigbigan/checkpoints/bs-128_save_name-normal_augment_data-basic/checkpoint_300.pth"

name_img = "cifar10_img_train_noattack_robbig_300ep"
name_lat = "cifar10_lat_train_noattack_robbig_300ep"

parser = argparse.ArgumentParser()
parser.add_argument("--dataset", type=str, default="CIFAR10",
                    choices=["FMNIST", "MNIST", "CIFAR10", "CIFAR100", "imagenette", "imagewoof"], help="dataset name")
parser.add_argument("--data_path", type=str, default="../input/CIFAR10",
                    help="path to dataset root folder")
parser.add_argument("--model_architecture", type=str, default="robbigbigan",
                    choices=["bigbiwgan", "bigbigan", "biggan", "classifier", "robbigbigan"], help="type of architecture used in training")
args = parser.parse_args()


def run_experiments():
    config = training_utils.get_config(args.dataset)
    hparams_str = ""
    config["model_architecture"] = args.model_architecture
    config["hparams_str"] = hparams_str.strip("_")
    config["seed"] = config.seed
    run_experiment(config)


def run_experiment(config):
    training_utils.set_random_seed(seed=config.seed, device=config.device)
    # training_pipeline = pipeline.BigBiGANInference.from_checkpoint(
    #     data_path=args.data_path,
    #     checkpoint_path=checkpoint_path,
    #     config=config
    # )
    # training_pipeline.inference()
    training_pipeline = pipeline.RobBigBiGANInference.from_checkpoint(
        data_path=args.data_path,
        checkpoint_path=checkpoint_path,
        config=config
    )
    # training_pipeline.inference()
    training_pipeline.generate_img_dataset(name=name_img)
    training_pipeline.generate_lat_dataset(name=name_lat)

run_experiments()
