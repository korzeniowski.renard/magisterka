import matplotlib.pyplot as plt
import numpy as np
import torch
from torchvision.datasets import FashionMNIST, MNIST
from torchvision import datasets, transforms
from torch.utils import data as tdataset
import torchvision.utils as vutils
from pathlib import Path

DATA_PATH = '../input/fmnist-dataset'
SAVE_PATH = './data/FMNIST/denseBiGAN/gen_img/'
lr = 0.002
bs = 256
epochs = 1000
z_dim = 64

img_dim = 784
d_h1 = 256
g_h1 = 256
e_h1 = 256


def weights_init(m):
    if isinstance(m, torch.nn.Linear):
        torch.nn.init.xavier(m.weight.data)
        torch.nn.init.xavier(m.bias.data)


class Gen(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.dense1 = torch.nn.Linear(z_dim, g_h1)
        self.relu = torch.nn.ReLU()
        self.dense2 = torch.nn.Linear(g_h1, img_dim)
        self.sigmoid = torch.nn.Sigmoid()
        weights_init(self)

    def forward(self, x):
        x = self.dense1(x)
        x = self.relu(x)
        x = self.dense2(x)
        return self.sigmoid(x)


class Disc(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.dense1 = torch.nn.Linear(img_dim + z_dim, d_h1)
        self.relu = torch.nn.ReLU()
        self.dense2 = torch.nn.Linear(d_h1, 1)
        self.sigmoid = torch.nn.Sigmoid()
        weights_init(self)

    def forward(self, x, z):
        x = torch.cat([x, z], dim=1)
        x = self.dense1(x)
        x = self.relu(x)
        x = self.dense2(x)
        return self.sigmoid(x)

    def requires_grad(self, req_grad):
        for p in self.parameters():
            p.requires_grad = req_grad


class Enc(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.dense1 = torch.nn.Linear(img_dim, e_h1)
        self.relu = torch.nn.ReLU()
        self.dense2 = torch.nn.Linear(e_h1, z_dim)
        weights_init(self)

    def forward(self, x):
        x = self.dense1(x)
        x = self.relu(x)
        x = self.dense2(x)
        return x



def save_img(epoch, img_gen):
    if epoch % 10 == 0:
        with torch.no_grad():
            fake = img_gen.detach().cpu()[:12, ...]
        fake_img = np.transpose(vutils.make_grid(
            fake, padding=2, nrow=3, normalize=True), (1, 2, 0))
        plt.imshow(fake_img)

        file_name = f"ep{epoch}.png"
        save_path = Path(SAVE_PATH)
        save_path.mkdir(parents=True, exist_ok=True)
        plt.savefig(
            fname=str(save_path / file_name),
        )


enc = Enc().cuda()
disc = Disc().cuda()
gen = Gen().cuda()

disc_optimizer = torch.optim.Adam(disc.parameters(), lr / 2)
gen_enc_optimizer = torch.optim.Adam(list(gen.parameters()) + list(enc.parameters()), lr)


dataset = FashionMNIST(
        DATA_PATH,
        download=True,
        transform=transforms.Compose([
            transforms.ToTensor(),
        ])
    )
data_loader = tdataset.DataLoader(
        dataset,
        batch_size=bs,
        shuffle=True,
        drop_last=True,
    )

n = len(data_loader)
print(n)
for epoch in range(epochs):
    gen_losses, disc_losses = 0, 0
    z = torch.rand(size=[bs, z_dim]).cuda() * 2 - 1
    for x, _ in data_loader:
        x = x.cuda()
        x = torch.flatten(x, start_dim=1)

        z_generated = enc.forward(x)
        x_generated = gen.forward(z)

        # optimize disc
        disc.requires_grad(True)
        disc.zero_grad()
        real_disc_output = disc.forward(x, z_generated.detach())
        generated_disc_output = disc.forward(x_generated.detach(), z)
        disc_loss = -1 * torch.mean(torch.log(real_disc_output + 1e-7) + torch.log(1.0 - generated_disc_output + 1e-7))
        disc_losses += disc_loss
        disc_loss.backward()
        disc_optimizer.step()

        # optimize gen, enc
        disc.requires_grad(False)
        gen_enc_optimizer.zero_grad()
        real_disc_output = disc.forward(x, z_generated)
        generated_disc_output = disc.forward(x_generated, z)
        gen_loss = -1 * torch.mean(torch.log(generated_disc_output + 1e-7) + torch.log(1.0 - real_disc_output + 1e-7))
        gen_losses += gen_loss
        gen_loss.backward()
        gen_enc_optimizer.step()

    print(disc_losses / n, gen_losses / n)
    save_img(epoch, x_generated)
