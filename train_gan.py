import argparse
import itertools

from src.model import architecture
from src.pipeline import pipeline
from src.pipeline import classification_pipeline
from src.pipeline import adversarial_pipeline
from src.training_utils import training_utils

EXP_HPARAMS = {
    "params": (
        {"bs": 128, "save_name": "normal", "augment_data": "basic"},
    ),
    "seeds": (420,),
}

parser = argparse.ArgumentParser()
parser.add_argument("--dataset", type=str, default="CIFAR10",
                    choices=["FMNIST", "MNIST", "CIFAR10", "CIFAR100", "imagenette", "imagewoof"], help="dataset name")
parser.add_argument("--data_path", type=str, default="../input/CIFAR10",
                    help="path to dataset root folder")
parser.add_argument("--model_architecture", type=str, default="bigbigan",
                    choices=["robgan", "robbigan", "robbigbigan", "bigbiwgan", "bigbigan", "biggan", "classifier",
                             "bigan", "adv_gan", "dcgan", "adv_lat", "repr_classifier", "attack_lat_clf", "attack_clf",
                             "robust_lat_clf", "repr_biggan_classifier", "repr_classifier"],
                    help="type of architecture used in training")
args = parser.parse_args()


def run_experiments():
    for hparams_overwrite_list, seed in itertools.product(EXP_HPARAMS["params"], EXP_HPARAMS["seeds"]):
        config = training_utils.get_config(args.dataset)
        hparams_str = ""
        for k, v in hparams_overwrite_list.items():
            config[k] = v
            hparams_str += str(k) + "-" + str(v) + "_"
        config["model_architecture"] = args.model_architecture
        config["hparams_str"] = hparams_str.strip("_")
        config["seed"] = seed
        run_experiment(config)


def run_experiment(config):
    training_utils.set_random_seed(seed=config.seed, device=config.device)
    if args.model_architecture == "bigbigan":
        training_pipeline = pipeline.BigBiGANPipeline.from_config(data_path=args.data_path, config=config)
    elif args.model_architecture == "bigan":
        training_pipeline = pipeline.BiGANPipeline.from_config(data_path=args.data_path, config=config)
    elif args.model_architecture == "biggan" or args.model_architecture == "dcgan":
        training_pipeline = pipeline.GANPipeline.from_config(data_path=args.data_path, config=config)
    ####
    elif args.model_architecture == "attack_lat_clf":
        training_pipeline = adversarial_pipeline.RobPGDAdversarialAttackPipeline.from_repr_checkpoint(
            data_path=args.data_path, config=config)
    elif args.model_architecture == "attack_clf":
        training_pipeline = adversarial_pipeline.RobPGDAdversarialAttackPipeline.from_pytorch(
            data_path=args.data_path, config=config)
    elif args.model_architecture == "robust_lat_clf":
        training_pipeline = adversarial_pipeline.RobustRobClfPipeline.from_checkpoint(
            arch=architecture.RobBigBiGAN, data_path=args.data_path, config=config)
    elif args.model_architecture == "repr_biggan_classifier":
        training_pipeline = classification_pipeline.RepresentationClassificationPipeline.from_config(
            arch=architecture.RobBigBiGAN, data_path=args.data_path, config=config)
    elif args.model_architecture == "repr_classifier":
        training_pipeline = classification_pipeline.RepresentationClassificationPipeline.from_config(
            arch=architecture.RobBigBiGAN, data_path=args.data_path, config=config)
    ####
    elif args.model_architecture == "classifier":
        training_pipeline = classification_pipeline.ClassificationPipeline.from_config(
            data_path=args.data_path, config=config)
    elif args.model_architecture == "adv_gan":
        training_pipeline = adversarial_pipeline.AdvAttackGeneratorGAN.from_config(
            data_path=args.data_path, config=config)
    elif args.model_architecture == "adv_lat":
        training_pipeline = adversarial_pipeline.LatentAdvAttackClassifier.from_config(
        data_path=args.data_path, config=config)
    elif args.model_architecture == "robgan":
        training_pipeline = pipeline.RobGANPipeline.from_config(data_path=args.data_path, config=config)
    elif args.model_architecture == "robbigan":
        training_pipeline = pipeline.RobBiGANPipeline.from_config(data_path=args.data_path, config=config)
    elif args.model_architecture == "robbigbigan":
        training_pipeline = pipeline.RobBigBiGANPipeline.from_config(data_path=args.data_path, config=config)

    else:
        raise ValueError(f"Architecture type {args.model_architecture} is not supported")
    training_pipeline.train_model()


run_experiments()
