import torch
from torch.optim.optimizer import Optimizer, required


class PGDAttacker:
    def __init__(self, criterion, adv_num_steps, step_norm, adv_step_size, eps_norm, adv_clamp):
        self.criterion = criterion
        self.adv_num_steps = adv_num_steps
        self.step_norm = step_norm
        self.adv_step_size = adv_step_size
        self.eps_norm = eps_norm
        self.adv_clamp = adv_clamp

    def projected_gradient_descent(self, x, y, eps, classifier, repr_model=None):
        x_adv = x.clone().detach().requires_grad_(True).to(x.device)
        num_channels = x.shape[1]

        for i in range(self.adv_num_steps):
            _x_adv = x_adv.clone().detach().requires_grad_(True)
            repr = _x_adv if repr_model is None else repr_model(_x_adv)
            prediction = classifier(repr)
            loss = self.criterion(prediction, y)
            loss.backward()

            with torch.no_grad():
                if self.step_norm == "inf":
                    gradients = _x_adv.grad.sign() * self.adv_step_size
                else:
                    gradients = _x_adv.grad * self.adv_step_size / _x_adv.grad.view(_x_adv.shape[0], -1) \
                        .norm(self.step_norm, dim=-1) \
                        .view(-1, num_channels, 1, 1)
                x_adv += gradients

            if self.eps_norm == "inf":
                x_adv = torch.max(torch.min(x_adv, x + eps), x - eps)
            else:
                delta = x_adv - x
                mask = delta.view(delta.shape[0], -1).norm(self.step_norm, dim=1) <= eps
                scaling_factor = delta.view(delta.shape[0], -1).norm(self.step_norm, dim=1)
                scaling_factor[mask] = eps
                delta *= eps / scaling_factor.view(-1, 1, 1, 1)
                x_adv = x + delta
            x_adv = x_adv.clamp(*self.adv_clamp)
        return x_adv.detach()


class AdvAttackerClfRob:
    def __init__(self, lr, epsilon, steps, proportion):
        self.lr = lr
        self.epsilon = epsilon
        self.steps = steps
        self.proportion = proportion

    def __call__(self, x, y, model, criterion, lat_enc=None):
        model.eval()
        x_adv = x.data.clone()
        x_adv = torch.autograd.Variable(x_adv, requires_grad=True)
        optimizer = Linf_SGD([x_adv], lr=self.lr)
        for _ in range(self.steps):
            optimizer.zero_grad()
            model.zero_grad()
            if lat_enc is not None:
                # with torch.no_grad():
                lat = lat_enc(x_adv)
                d_multi = model(lat)
            else:
                d_multi = model(x_adv)
            loss = -criterion(d_multi, y)
            loss.backward()
            optimizer.step()
            diff = x_adv.data - x.data
            diff.clamp_(-self.epsilon, self.epsilon)
            x_adv.data.copy_((diff + x.data).clamp_(-1, 1))
        model.train()
        model.zero_grad()
        return x_adv


class AdvAttackerRob:
    def __init__(self, lr, epsilon, steps, proportion):
        self.lr = lr
        self.epsilon = epsilon
        self.steps = steps
        self.proportion = proportion

    def __call__(self, x, y, model, criterion):
        model.eval()
        ones = torch.ones_like(y).float()
        x_adv = x.data.clone()
        x_adv = torch.autograd.Variable(x_adv, requires_grad=True)
        optimizer = Linf_SGD([x_adv], lr=self.lr)
        for _ in range(self.steps):
            optimizer.zero_grad()
            model.zero_grad()
            d_bin, d_multi = model(x_adv)
            loss = -criterion(d_bin, ones, d_multi, y)
            loss.backward()
            optimizer.step()
            diff = x_adv.data - x.data
            diff.clamp_(-self.epsilon, self.epsilon)
            x_adv.data.copy_((diff + x.data).clamp_(-1, 1))
        model.train()
        model.zero_grad()
        return x_adv


class AdvAttackerBiRob:
    def __init__(self, lr, epsilon, steps, proportion):
        self.lr = lr
        self.epsilon = epsilon
        self.steps = steps
        self.proportion = proportion

    def __call__(self, x, y, model, encoder, criterion):
        model.eval()
        ones = torch.ones_like(y).float()
        x_adv = x.data.clone()
        x_adv = torch.autograd.Variable(x_adv, requires_grad=True)
        optimizer = Linf_SGD([x_adv], lr=self.lr)
        for _ in range(self.steps):
            optimizer.zero_grad()
            model.zero_grad()
            z = encoder(x_adv)
            d_bin, d_multi = model(x_adv, z)
            loss = -criterion.forward_adv(d_bin, ones, d_multi, y)
            loss.backward()
            optimizer.step()
            diff = x_adv.data - x.data
            diff.clamp_(-self.epsilon, self.epsilon)
            x_adv.data.copy_((diff + x.data).clamp_(-1, 1))
        model.train()
        model.zero_grad()
        return x_adv


class AdvAttackerBigBiRob:
    def __init__(self, lr, epsilon, steps, proportion):
        self.lr = lr
        self.epsilon = epsilon
        self.steps = steps
        self.proportion = proportion

    def __call__(self, x, y, model, criterion):
        model.eval()
        x_adv = x.data.clone()
        x_adv = torch.autograd.Variable(x_adv, requires_grad=True)
        optimizer = Linf_SGD([x_adv], lr=self.lr)
        for _ in range(self.steps):
            optimizer.zero_grad()
            model.zero_grad()
            z_img = model.encoder(x_adv)
            output = model.forward_real(x_adv, z_img)
            loss = -criterion.forward_adv(output, y)
            loss.backward()
            optimizer.step()
            diff = x_adv.data - x.data
            diff.clamp_(-self.epsilon, self.epsilon)
            x_adv.data.copy_((diff + x.data).clamp_(-1, 1))
        model.train()
        model.zero_grad()
        return x_adv


class Linf_SGD(Optimizer):
    def __init__(self, params, lr=required, momentum=0, dampening=0,
                 weight_decay=0, nesterov=False):
        defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
                        weight_decay=weight_decay, nesterov=nesterov)
        if nesterov and (momentum <= 0 or dampening != 0):
            raise ValueError("Nesterov momentum requires a momentum and zero dampening")
        super(Linf_SGD, self).__init__(params, defaults)

    def __setstate__(self, state):
        super(Linf_SGD, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('nesterov', False)

    def step(self, closure=None):
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            weight_decay = group['weight_decay']
            momentum = group['momentum']
            dampening = group['dampening']
            nesterov = group['nesterov']

            for p in group['params']:
                if p.grad is None:
                    continue
                d_p = torch.sign(p.grad.data)
                if weight_decay != 0:
                    d_p.add_(weight_decay, p.data)
                if momentum != 0:
                    param_state = self.state[p]
                    if 'momentum_buffer' not in param_state:
                        buf = param_state['momentum_buffer'] = torch.zeros_like(p.data)
                        buf.mul_(momentum).add_(d_p)
                    else:
                        buf = param_state['momentum_buffer']
                        buf.mul_(momentum).add_(1 - dampening, d_p)
                    if nesterov:
                        d_p = d_p.add(momentum, buf)
                    else:
                        d_p = buf

                p.data.add_(-group['lr'], d_p)

        return loss