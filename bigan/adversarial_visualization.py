# 0. load model
# 1. make attack
# 2. show original image, generator's output with this image encoding, generator's output with adv attacked image encoding


import matplotlib.pyplot as plt
import argparse
import torch
from train import TrainerBiGAN
from preprocess import get_cifar10
from barbar import Bar
import torchvision.utils as vutils

epochs = 100
class_count = 10
representation_dim = 256
eps = 0.1
attack_type = 'inf'

parser = argparse.ArgumentParser()
parser.add_argument("--num_epochs", type=int, default=200,
                    help="number of epochs")
parser.add_argument('--lr_adam', type=float, default=1e-4,
                    help='learning rate')
parser.add_argument('--lr_rmsprop', type=float, default=1e-4,
                    help='learning rate RMSprop if WGAN is True.')
parser.add_argument("--batch_size", type=int, default=128,
                    help="Batch size")
parser.add_argument('--latent_dim', type=int, default=256,
                    help='Dimension of the latent variable z')
parser.add_argument('--wasserstein', type=bool, default=False,
                    help='If WGAN.')
parser.add_argument('--clamp', type=float, default=1e-2,
                    help='Clipping gradients for WGAN.')
parser.add_argument('--start_epoch', type=int, default=0,
                    help='Start epoch based on checkpoint.')
parser.add_argument('--checkpoint', type=str, default=None,
                    help='Path to checkpoint.')
parser.add_argument('--head_checkpoint', type=str, default=None,
                    help='Path to checkpoint.')

#parsing arguments.
args = parser.parse_args()


class RepresentationClassifier(torch.nn.Module):
    def __init__(self, head, encoder):
        super().__init__()
        self.head = head
        self.encoder = encoder

    def forward(self, x, with_enc_grads=False):
        if with_enc_grads:
            enc_x = encoder(x).squeeze()
        else:
            with torch.no_grad():
                enc_x = encoder(x).squeeze()

        outputs = net(enc_x)
        return outputs


def perform_attack(data, model, criterion, device, eps, attack_type='inf'):
    for i, (x, y) in enumerate(Bar(data)):
        x = x.to(device)
        x_adv = projected_gradient_descent(model, x, y, criterion,
                                           num_steps=40, step_size=0.01,
                                           eps=eps, eps_norm=attack_type,
                                           step_norm=attack_type)
        return x, x_adv


def projected_gradient_descent(model, x, y, loss_fn, num_steps, step_size, step_norm, eps, eps_norm,
                               clamp=(0, 1)):
    """Performs the projected gradient descent attack on a batch of images."""
    x_adv = x.clone().detach().requires_grad_(True).to(x.device)
    num_channels = x.shape[1]

    for i in range(num_steps):
        _x_adv = x_adv.clone().detach().requires_grad_(True)

        prediction = model(_x_adv, with_enc_grads=True)

        y = y.to(device)
        loss_fn = loss_fn.to(device)
        loss = loss_fn(prediction, y)
        loss.backward()

        with torch.no_grad():
            # Force the gradient step to be a fixed size in a certain norm
            if step_norm == 'inf':
                gradients = _x_adv.grad.sign() * step_size
            else:
                # Note .view() assumes batched image data as 4D tensor
                gradients = _x_adv.grad * step_size / _x_adv.grad.view(_x_adv.shape[0], -1) \
                    .norm(step_norm, dim=-1) \
                    .view(-1, num_channels, 1, 1)

            # Untargeted: Gradient ascent on the loss of the correct label w.r.t.
            # the model parameters
            x_adv += gradients

        # Project back into l_norm ball and correct range
        if eps_norm == 'inf':
            # Workaround as PyTorch doesn't have elementwise clip
            x_adv = torch.max(torch.min(x_adv, x + eps), x - eps)
        else:
            delta = x_adv - x

            # Assume x and x_adv are batched tensors where the first dimension is
            # a batch dimension
            mask = delta.view(delta.shape[0], -1).norm(step_norm, dim=1) <= eps

            scaling_factor = delta.view(delta.shape[0], -1).norm(step_norm, dim=1)
            scaling_factor[mask] = eps

            # .view() assumes batched images as a 4D Tensor
            delta *= eps / scaling_factor.view(-1, 1, 1, 1)

            x_adv = x + delta

        x_adv = x_adv.clamp(*clamp)

    return x_adv.detach()

def visualize_attack_effects():
    pass


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
net = torch.nn.Sequential(
    torch.nn.Linear(representation_dim, representation_dim // 2),
    torch.nn.ReLU(),
    torch.nn.Linear(representation_dim // 2, class_count),
)
net.load_state_dict(torch.load(args.head_checkpoint, map_location=torch.device(device)))
criterion = torch.nn.CrossEntropyLoss(reduce='sum')

data = get_cifar10(args)
bigan = TrainerBiGAN(args, data, device)
bigan.model.load_state_dict(torch.load(args.checkpoint, map_location=torch.device(device)))
bigan.model.eval()
encoder = bigan.model.E
generator = bigan.model.G

model = RepresentationClassifier(head=net, encoder=encoder).to(device)

x, x_adv = perform_attack(
    model=model, data=data, eps=eps, attack_type=attack_type, device=device, criterion=criterion)

enc_x = encoder(x)
pred_img = generator(enc_x)

adv_enc_x = encoder(x_adv)
pred_adv_img = generator(adv_enc_x)

vutils.save_image(x.data, 'original_img.png')
vutils.save_image(x_adv.data, 'adv_img.png')
vutils.save_image(pred_img.data, 'reconstructed_img.png')
vutils.save_image(pred_adv_img.data, 'adv_reconstructed_img.png')
