import torch
from torchvision import models

from src.model import layers


class ClassifierHead(torch.nn.Module):
    def __init__(self, clf_head):
        super().__init__()
        self.clf_head = clf_head

    def forward(self, x):
        y = self.clf_head(x)
        return y

    @classmethod
    def from_config(cls, config):
        clf_head = torch.nn.Sequential(
            torch.nn.Linear(config.latent_dim, config.latent_dim),
            torch.nn.BatchNorm1d(config.latent_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(config.latent_dim, config.latent_dim),
            torch.nn.BatchNorm1d(config.latent_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(config.latent_dim, config.latent_dim),
            torch.nn.BatchNorm1d(config.latent_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(config.latent_dim, config.latent_dim),
            torch.nn.BatchNorm1d(config.latent_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(config.latent_dim, config.latent_dim),
            torch.nn.BatchNorm1d(config.latent_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(config.latent_dim, config.latent_dim // 2),
            torch.nn.BatchNorm1d(config.latent_dim // 2),
            torch.nn.ReLU(),
            torch.nn.Linear(config.latent_dim // 2, config.num_cls),
            # torch.nn.Sigmoid(),
        )
        return cls(
            clf_head=clf_head,
        )


class ResNet(torch.nn.Module):
    def __init__(self, resnet, mlp_dim, dropout, w_init):
        super().__init__()
        in_dim = resnet.fc.in_features
        resnet.fc = layers.LinearResnetBlock(in_dim, mlp_dim, dropout, sn=False, w_init=w_init)
        self.model = resnet

    def forward(self, x):
        return self.model(x)

    @classmethod
    def from_config(cls, config):
        model_version = config.clf_model
        if model_version == "resnet_18":
            resnet = models.resnet18(pretrained=config.clf_pretrained, progress=True)
        elif model_version == "resnet_50":
            resnet = models.resnet50(pretrained=config.clf_pretrained, progress=True)
        else:
            raise ValueError("This type of encoder is not supported")

        return cls(
            resnet=resnet,
            mlp_dim=config.num_cls,
            dropout=config.dropout,
            w_init=config.w_init
        )
