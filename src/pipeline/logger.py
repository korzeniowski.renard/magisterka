from torch.utils.tensorboard import SummaryWriter


class Logger:
    def __init__(self, log_dir, logging_interval):
        self.writer = SummaryWriter(log_dir)
        self.logging_interval = logging_interval
        self.counter = 0

    @classmethod
    def from_config(cls, config, name):
        log_dir = config.logging_path.format(
            ds_name=config.ds_name,
            model_architecture=config.model_architecture,
            name=name
        )
        return cls(log_dir=log_dir, logging_interval=config.logging_interval)


class BiGANLogger(Logger):
    def __call__(self, epoch, step, logging_dict, *args, **kwargs):
        self.counter += 1
        if step % self.logging_interval == 0:
            for k, v in logging_dict.items():
                if v is not None:
                    self.writer.add_scalar(f'{k}', v, self.counter)
            print(f"epoch {epoch}, disc_loss {logging_dict['disc_loss']}, gen_enc_loss {logging_dict['gen_loss']}")
            if "disc_clf_acc" in logging_dict.keys() and "gen_clf_acc" in logging_dict.keys():
                print(f"disc_clf_acc {logging_dict['disc_clf_acc']}, gen_clf_acc {logging_dict['gen_clf_acc']}")


class GANLogger(Logger):
    def __call__(self, epoch, step, logging_dict, *args, **kwargs):
        self.counter += 1
        if step % self.logging_interval == 0:
            output_string = ""
            for k, v in logging_dict.items():
                if v is not None:
                    self.writer.add_scalar(f'{k}', v, self.counter)
                    output_string += f"{k}: {v}, "

            print(f'epoch {epoch} {output_string}')


class AdvGANLogger(Logger):
    def __call__(self, epoch, step, disc_loss, gen_loss, gen_disc_acc,
                 disc_real_acc, disc_fake_acc, adv_loss, l2_loss, acc, adv_acc, *args, **kwargs):
        self.counter += 1
        if step % self.logging_interval == 0:
            self.writer.add_scalar(f'Loss/Disc', disc_loss, self.counter)
            self.writer.add_scalar(f'Loss/Gen', gen_loss, self.counter)
            self.writer.add_scalar(f'Gen/DiscFake', gen_disc_acc, self.counter)
            self.writer.add_scalar(f'Disc/Real', disc_real_acc, self.counter)
            self.writer.add_scalar(f'Disc/Fake', disc_fake_acc, self.counter)
            self.writer.add_scalar(f'adv_loss', adv_loss, self.counter)
            self.writer.add_scalar(f'l2_loss', l2_loss, self.counter)
            print(f"epoch {epoch}, disc_loss {disc_loss}, gen_loss {gen_loss}")
            print(f"gen_disc_acc {gen_disc_acc}, disc_real_acc {disc_real_acc}, disc_fake_acc {disc_fake_acc}")
            print(f"adv_loss {adv_loss}, adv_mask_l2_loss {l2_loss}")
            print(f"batch acc {acc}, adv_acc {adv_acc}")


class EvalAdvGANLogger(Logger):
    def __call__(self, step, adv_loss, l2_loss, clf_acc, clf_adv_acc, *args, **kwargs):
        self.counter += 1
        self.writer.add_scalar(f'eval_adv_loss', adv_loss, self.counter)
        self.writer.add_scalar(f'eval_l2_loss', l2_loss, self.counter)
        self.writer.add_scalar(f'eval_clf_acc', clf_acc, self.counter)
        self.writer.add_scalar(f'eval_clf_adv_acc', clf_adv_acc, self.counter)
        print(f"adv_loss {adv_loss}, adv_mask_l2_loss {l2_loss}, clf_acc {clf_acc}, clf_adv_acc {clf_adv_acc}")


class ClfLogger(Logger):
    def __call__(self, epoch, loss, step, logging_dict=None, *args, **kwargs):
        self.counter += 1
        if step % self.logging_interval == 0:
            self.writer.add_scalar('Loss', loss, self.counter)
            print(f"epoch {epoch}, loss {loss}")

            if logging_dict is not None:
                for k, v in logging_dict.items():
                    self.writer.add_scalar(f'{k}', v, self.counter)
