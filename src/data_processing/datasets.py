import os

import pandas as pd
from torchvision.datasets import ImageFolder


class Imagenette(ImageFolder):
    def __init__(self, root, train, csv="noisy_imagenette.csv", **kwargs):
        if train:
            data_path = os.path.join(root, 'train')
        else:
            data_path = os.path.join(root, 'test')
        super().__init__(data_path, **kwargs)
        csv_path = os.path.join(root, csv)
        # filter CSV by column so its only from train or test set
        ds = pd.read_csv(csv_path)
        self.classes = list(set(ds.noisy_labels_0))
        self.class_to_idx = {cls: idx for idx, cls in enumerate(self.classes)}
        self.imgs = self.get_imgs(root, ds, train)

    def get_imgs(self, root, ds, train):
        return [(os.path.join(root, path), self.class_to_idx[target])
                for path, target, is_valid in zip(ds.path, ds.noisy_labels_0, ds.is_valid)
                if is_valid != train]
