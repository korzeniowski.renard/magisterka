import random
import sys

import numpy as np
from scipy.stats import truncnorm
import torch


class Config:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key] = value


def get_config(dataset):
    config_module = "src.configs." + dataset
    __import__(config_module)
    config = sys.modules[config_module].config
    return Config(**{key: value for key, value in config.__dict__.items()
                     if not (key.startswith('__') or key.startswith('_'))})


def truncated_normal(size, threshold=1):
    values = truncnorm.rvs(-threshold, threshold, size=size)
    return torch.from_numpy(values)


def get_channel_inputs(array, input_dim=None, output_dim=None):
    if input_dim is not None:
        array = [input_dim] + list(array)
    if output_dim is not None:
        array = list(array) + [output_dim]
    return list(zip(array[:-1], array[1:]))


def set_random_seed(seed, device):
    np.random.seed(seed)
    torch.manual_seed(seed)
    random.seed(seed)
    if device == "cuda":
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False


def weights_init_normal(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1 and classname != 'Conv':
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
        if m.bias is not None:
            m.bias.data.fill_(0)
    elif classname.find("Linear") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
        if m.bias is not None:
            m.bias.data.fill_(0)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.01)
        if m.bias is not None:
            m.bias.data.fill_(0)


def calculate_acc(y, y_hat):
    _, idx = torch.max(y_hat.data, dim=1)
    correct_real = torch.sum(idx.eq(y)).item()
    return correct_real / y.shape[0]
