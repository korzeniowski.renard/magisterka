import argparse
import torch
from train import TrainerBiGAN
from preprocess import get_cifar10

CHECKPOINT_PATH = './default_checkpoint_vanillaBiGAN_clf_epoch_{}.pt'

parser = argparse.ArgumentParser()
parser.add_argument("--num_epochs", type=int, default=200,
                    help="number of epochs")
parser.add_argument('--lr_adam', type=float, default=1e-4,
                    help='learning rate')
parser.add_argument('--lr_rmsprop', type=float, default=1e-4,
                    help='learning rate RMSprop if WGAN is True.')
parser.add_argument("--batch_size", type=int, default=128,
                    help="Batch size")
parser.add_argument('--latent_dim', type=int, default=256,
                    help='Dimension of the latent variable z')
parser.add_argument('--wasserstein', type=bool, default=False,
                    help='If WGAN.')
parser.add_argument('--clamp', type=float, default=1e-2,
                    help='Clipping gradients for WGAN.')
parser.add_argument('--start_epoch', type=int, default=0,
                    help='Start epoch based on checkpoint.')
parser.add_argument('--checkpoint', type=str, default=None,
                    help='Path to checkpoint.')

#parsing arguments.
args = parser.parse_args()

epochs = 400
class_count = 10
representation_dim = 256


class RepresentationClassifier(torch.nn.Module):
    def __init__(self, head, encoder):
        super().__init__()
        self.head = head
        self.encoder = encoder

    def forward(self, x, with_enc_grads=True):
        if with_enc_grads:
            enc_x = self.encoder(x).squeeze()
        else:
            with torch.no_grad():
                enc_x = self.encoder(x).squeeze()

        outputs = self.head(enc_x)
        return outputs


def calculate_acc(preds, labels):
    preds = preds.max(dim=1)[1].numpy()
    labels = labels.numpy()
    return sum(preds == labels) / len(labels)


def train_model(data, epochs, model, device):
    for epoch in range(epochs):
        running_acc = 0
        best_acc = 0
        for i, (x, y) in enumerate(data):
            x = x.to(device)
            optimizer.zero_grad()
            outputs = model(x).cpu()
            loss = criterion(outputs, y)
            loss.backward()
            optimizer.step()

            running_acc += calculate_acc(preds=outputs, labels=y)
            if i % 50 == 0:
                print(f"epoch {epoch + 1}, batch {i + 1}, loss {loss}")
                print(f"accuracy {running_acc / (i + 1)}")
        if epoch % 50:
            torch.save(net.state_dict(), CHECKPOINT_PATH.format(epoch))

        best_acc = max(best_acc, running_acc)
        print(f"best_acc {best_acc / (i + 1)}")


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
net = torch.nn.Sequential(
    torch.nn.Linear(representation_dim, representation_dim // 2),
    torch.nn.ReLU(),
    torch.nn.Linear(representation_dim // 2, class_count),
).to(device)

criterion = torch.nn.CrossEntropyLoss(reduce='sum')
optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

data = get_cifar10(args)
bigan = TrainerBiGAN(args, data, device)
encoder = bigan.model.E

model = RepresentationClassifier(head=net, encoder=encoder).to(device)

train_model(epochs=epochs, data=data, model=model, device=device)
