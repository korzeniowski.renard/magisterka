import argparse

from src.pipeline import classification_pipeline
from src.training_utils import training_utils

checkpoint_path = "/home/rkorzeniowski/magisterka/data/CIFAR10_newest/bigbigan/checkpoints/checkpoint_300.pth"

parser = argparse.ArgumentParser()
parser.add_argument("--dataset", type=str, default="FMNIST",
                    choices=["FMNIST", "MNIST", "CIFAR10", "CIFAR100", "imagenette", "imagewoof"], help="dataset name")
parser.add_argument("--data_path", type=str, default="../input/fmnist-dataset",
                    help="path to dataset root folder")
parser.add_argument("--model_architecture", type=str, default="bigbigan",
                    choices=["bigbiwgan", "bigbigan", "biggan", "classifier"], help="type of architecture used in training")
args = parser.parse_args()


def run_experiments():
    config = training_utils.get_config(args.dataset)
    hparams_str = ""
    config["model_architecture"] = args.model_architecture
    config["hparams_str"] = hparams_str.strip("_")
    config["seed"] = config.seed
    run_experiment(config)


def run_experiment(config):
    training_utils.set_random_seed(seed=config.seed, device=config.device)
    training_pipeline = classification_pipeline.RepresentationClassificationPipeline.from_repr_checkpoint(
        data_path=args.data_path,
        checkpoint_path=checkpoint_path,
        config=config
    )
    training_pipeline.train_model()


run_experiments()
