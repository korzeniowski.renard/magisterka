hparams = {
    # training utils
    "seed": 420,
    "device": "cuda",
    "img_rows": 4,
    "save_img_count": 12,
    "real_imgs_save_path": "./data/{ds_name}/{model_architecture}/real_img/{hparams}",
    "gen_imgs_save_path": "./data/{ds_name}/{model_architecture}/gen_img/{hparams}",
    "logging_path": "./data/{ds_name}/{model_architecture}/logs/{name}",
    "save_model_path": "./data/{ds_name}/{model_architecture}/checkpoints/{hparams}",
    "rec_imgs_save_path": "./data/{ds_name}/{model_architecture}/reconstructions/{hparams}",
    "save_name": "gan",
    "save_model_interval": 50,

    # hparams
    "clf_lr": 2e-5,
    "disc_steps": 2,
    "gen_steps": 1,
    "epochs": 2000,
    "lr_enc": 2e-3,
    "lr_gen": 8e-4,
    "lr_disc": 2e-4,
    "betas": (0.5, 0.999),
    "scale_grad_by_steps": True,

    # model params
    "dropout": 0.2,
    "latent_loss": True,
    "spectral_norm": True,
    "weight_cutoff": 0.00,
    "add_noise": 0,

    # classifier
    "classifier_checkpoint_path": "/home/ec2-user/renard/bigbigan/data/CIFAR10/classifier/checkpoints/bs-128_save_name-normal_augment_data-basic_baseline_noattack-True/checkpoint_classifier_10.pth",
    "save_clf_interval": 2,
    "clf_model": "resnet_50",
    "clf_pretrained": True,

    # adv attacks
    "repr_checkpoint_path": "/home/ec2-user/renard/bigbigan/data/CIFAR10/robbigbigan/checkpoints/bs-128_save_name-normal_augment_data-basic_baseline_noattack-True/checkpoint_200.pth",
    # "/home/ec2-user/renard/bigbigan/data/CIFAR10/robbigbigan/checkpoints/bs-128_save_name-normal_augment_data-basic/checkpoint_250.pth",
    "directed_attack": False,
    "pixel_change_coeff": 50,
    "cls_checkpoint_path": "/home/ec2-user/renard/bigbigan/data/imagewoof/classifier/checkpoints/checkpoint_classifier_40.pth",
    "save_adv_img_interval": 1,
    "eps": 0.1,

    "eps_list": [0.0, 0.01, 0.05, 0.1, 0.2, 0.5],
    "adv_num_steps": 40,
    "step_norm": 'inf',
    "eps_norm": 'inf',
    "adv_step_size": 0.01,
    "adv_clamp": (0, 1),
    "adv_attacks_imgs_save_path": "/home/ec2-user/renard/bigbigan/data/{ds_name}_disc/{model_architecture}/adv_attack/{hparams}",

    # robgan
    "baseline_noattack": False,
    "rob_lr": 0.0002,
    "rob_ch": 64,
    "rob_z_dim": 128,
    "bottom_width": 4,
    "start_width": 4,
    "rob_adv_lr": 0.0078 / 2,
    "rob_adv_epsilon": 0.03125,
    "rob_adv_steps": 5,
    "rob_proportion": 0.5,

    # latent clf
    "clf_checkpoint_path": "/home/ec2-user/renard/bigbigan/data/CIFAR10/classifier/checkpoints/bs-128/checkpoint_classifier_10.pth",
    "clf_head_checkpoint_path": "/home/ec2-user/renard/bigbigan/data/CIFAR10/repr_classifier/checkpoints/bs-128/checkpoint_repr_classifier_20.pth",

    # augmentation
    "augment_data": "basic",
    "horizontal_flip_p": 0.5,
    "color_jitter": (0.0, 0.1),
    "deg_rotation": 30,
    "affine_translate": (0.1, 0.1),
    "perspective_dist_scale": 0.2,
    "perspective_p": 0.25,
    "random_erase_p": 0.2,
    "random_erase_scale": (0.02, 0.2),
    "random_erase_ratio": (0.3, 3.3),

    # bigan
    "wasserstein": False,
    "clamp": 1e-2,
    "bigan_latent": 256,
}
