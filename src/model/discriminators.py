import torch
import torch.nn as nn
import torch.nn.functional as F

from src.model import layers
from src.model import rob_layers
from src.training_utils import training_utils

class WDisc(nn.Module):
    def clip_weights(self, weight_cutoff):
        for param in self.parameters():
            param.data.clamp_(-weight_cutoff, weight_cutoff)


class DiscBigGAN(nn.Module):
    def __init__(self, mult_chs, ks, num_cls, sn, w_init):
        super().__init__()

        m_pre_chs, m_post_chs, colors = mult_chs["pre"], mult_chs["post"], mult_chs["colors"]
        resblocks_output = m_post_chs[-1]
        m_post_chs = training_utils.get_channel_inputs(m_post_chs, input_dim=m_pre_chs[-1])
        m_pre_chs = training_utils.get_channel_inputs(m_pre_chs, input_dim=colors)

        self.pre_down_blocks = nn.Sequential(*[
            layers.DownResnetBlock(in_ch=in_m, out_ch=out_m, ks=ks, sn=sn, bias=False, w_init=w_init)
            for in_m, out_m in m_pre_chs
        ]) # tf 64 -> # here 64

        self.non_loc = layers.SelfAttn(mult_chs["pre"][-1], sn=sn) # tf 64 # here 64
        self.post_down_blocks = nn.Sequential(*[
            layers.DownResnetBlock(in_ch=in_m, out_ch=out_m, ks=ks, sn=sn, bias=False, w_init=w_init)
            for in_m, out_m in m_post_chs
        ]) # tf -> 128 -> 256 # here 128 -> 256

        self.res_block = layers.ConstResnetBlock(resblocks_output, resblocks_output, ks, sn=sn, bias=False, w_init=w_init)
        self.relu = nn.ReLU()
        self.linear = nn.Linear(in_features=resblocks_output, out_features=1)
        self.cls_embedding = nn.Embedding(num_embeddings=num_cls, embedding_dim=resblocks_output) # does this embedding dim should be different than the generetor ones?

        if w_init is not None: w_init(self.linear.weight)

    def forward(self, x, cls):
        y = self.pre_down_blocks(x)
        y = self.non_loc(y)
        y = self.post_down_blocks(y)
        y = self.res_block(y)
        y = self.relu(y)
        y1 = torch.sum(y, dim=[-2, -1])
        y2 = self.linear(y1)

        if cls is not None:
            cls_embed = self.cls_embedding(cls)
            y2 = y2 + torch.sum(cls_embed * y1, dim=-1, keepdim=True)

        return y1, y2

    @classmethod
    def from_config(cls, config):
        return cls(
            mult_chs=config.disc_mult_chs,
            ks=config.ks,
            num_cls=config.num_cls,
            w_init=config.w_init,
            sn=config.spectral_norm,
        )


class DiscBigWGAN(DiscBigGAN, WDisc):
    pass


class DiscMLP(nn.Module):
    def __init__(self, n_blocks, mlp_dim, in_dim, dropout, sn, w_init):
        super().__init__()
        mlp = [layers.LinearResnetBlock(in_dim=mlp_dim, out_dim=mlp_dim, dropout=dropout, sn=sn, w_init=w_init)
               for _ in range(n_blocks - 1)]
        mlp = [layers.LinearResnetBlock(in_dim=in_dim, out_dim=mlp_dim, dropout=dropout, sn=sn, w_init=w_init)] + mlp
        self.mpls = nn.Sequential(*mlp)
        self.linear = nn.Linear(in_features=mlp_dim, out_features=1)
        if w_init is not None: w_init(self.linear.weight)


class LatentDisc(DiscMLP):
    def forward(self, z):
        y1 = self.mpls(z.float())
        y2 = self.linear(y1)
        return y1, y2

    @classmethod
    def from_config(cls, config):
        return cls(
            n_blocks=config.latent_disc_blocks,
            mlp_dim=config.latent_disc_mlp_dim,
            in_dim=config.latent_dim,
            dropout=config.dropout,
            w_init=config.w_init,
            sn=config.spectral_norm,
        )


class LatentWDisc(LatentDisc, WDisc):
    pass


class CombDisc(DiscMLP):
    def forward(self, img, latent):
        x = torch.cat([img, latent], dim=-1)
        y = self.mpls(x)
        y = self.linear(y)
        return y

    @classmethod
    def from_config(cls, config):
        return cls(
            n_blocks=config.comb_disc_blocks,
            mlp_dim=config.comb_disc_mlp_dim,
            in_dim=config.latent_disc_mlp_dim + config.disc_mult_chs["post"][-1],
            dropout=config.dropout,
            w_init=config.w_init,
            sn=config.spectral_norm,
        )


class CombWDisc(CombDisc, WDisc):
    pass


class BiGANDiscriminator(nn.Module):
    def __init__(self, z_dim=32, wasserstein=False):
        super(BiGANDiscriminator, self).__init__()
        self.wass = wasserstein

        # Inference over x
        self.conv1x = nn.Conv2d(3, 32, 5, stride=1, bias=False)
        self.conv2x = nn.Conv2d(32, 64, 4, stride=2, bias=False)
        self.bn2x = nn.BatchNorm2d(64)
        self.conv3x = nn.Conv2d(64, 128, 4, stride=1, bias=False)
        self.bn3x = nn.BatchNorm2d(128)
        self.conv4x = nn.Conv2d(128, 256, 4, stride=2, bias=False)
        self.bn4x = nn.BatchNorm2d(256)
        self.conv5x = nn.Conv2d(256, 512, 4, stride=1, bias=False)
        self.bn5x = nn.BatchNorm2d(512)

        # Inference over z
        self.conv1z = nn.Conv2d(z_dim, 512, 1, stride=1, bias=False)
        self.conv2z = nn.Conv2d(512, 512, 1, stride=1, bias=False)

        # Joint inference
        self.conv1xz = nn.Conv2d(1024, 1024, 1, stride=1, bias=False)
        self.conv2xz = nn.Conv2d(1024, 1024, 1, stride=1, bias=False)
        self.conv3xz = nn.Conv2d(1024, 1, 1, stride=1, bias=False)

    def inf_x(self, x):
        x = F.dropout2d(F.leaky_relu(self.conv1x(x), negative_slope=0.1), 0.2)
        x = F.dropout2d(F.leaky_relu(self.bn2x(self.conv2x(x)), negative_slope=0.1), 0.2)
        x = F.dropout2d(F.leaky_relu(self.bn3x(self.conv3x(x)), negative_slope=0.1), 0.2)
        x = F.dropout2d(F.leaky_relu(self.bn4x(self.conv4x(x)), negative_slope=0.1), 0.2)
        x = F.dropout2d(F.leaky_relu(self.bn5x(self.conv5x(x)), negative_slope=0.1), 0.2)
        return x

    def inf_z(self, z):
        z = F.dropout2d(F.leaky_relu(self.conv1z(z), negative_slope=0.1), 0.2)
        z = F.dropout2d(F.leaky_relu(self.conv2z(z), negative_slope=0.1), 0.2)
        return z

    def inf_xz(self, xz):
        xz = F.dropout(F.leaky_relu(self.conv1xz(xz), negative_slope=0.1), 0.2)
        xz = F.dropout(F.leaky_relu(self.conv2xz(xz), negative_slope=0.1), 0.2)
        return self.conv3xz(xz)

    def forward(self, x, z):
        x = self.inf_x(x)
        z = self.inf_z(z)
        xz = torch.cat((x,z), dim=1)
        out = self.inf_xz(xz)
        if self.wass:
            return out
        else:
            return torch.sigmoid(out)


class ResNetAC(nn.Module):
    def __init__(self, ch, n_classes, activation=F.relu, bn=True):
        super().__init__()
        self.activation = activation
        self.block1 = rob_layers.DownOptimizedBlock(3, ch * 2, bn=bn)
        self.block2 = rob_layers.DownBlock(ch * 2, ch * 2, activation=activation, downsample=True, bn=bn)
        self.block3 = rob_layers.DownBlock(ch * 2, ch * 2, activation=activation, downsample=False, bn=bn)
        self.block4 = rob_layers.DownBlock(ch * 2, ch * 2, activation=activation, downsample=False, bn=bn)
        self.l5 = nn.Linear(ch * 2, 1)
        nn.init.xavier_uniform_(self.l5.weight, gain=1.0)
        if n_classes > 0:
            self.l_y = nn.Linear(ch * 2, n_classes)
            nn.init.xavier_uniform_(self.l_y.weight, gain=1.0)

    def forward(self, x):
        h = x
        h = self.block1(h)
        h = self.block2(h)
        h = self.block3(h)
        h = self.block4(h)
        h = self.activation(h)
        h = h.view(h.size(0), h.size(1), -1)
        h = torch.sum(h, 2)
        output = self.l5(h)
        w_y = self.l_y(h)
        return output.view(-1), w_y


class ResNetBiAC(nn.Module):
    def __init__(self, ch, z_dim, n_classes, activation=F.relu, bn=True):
        super().__init__()
        self.activation = activation
        self.block1 = rob_layers.DownOptimizedBlock(3, ch * 2, bn=bn)
        self.block2 = rob_layers.DownBlock(ch * 2, ch * 2, activation=activation, downsample=True, bn=bn)
        self.block3 = rob_layers.DownBlock(ch * 2, ch * 2, activation=activation, downsample=False, bn=bn)
        self.block4 = rob_layers.DownBlock(ch * 2, 256, activation=activation, downsample=False, bn=bn)
        # self.l5 = nn.Linear(ch * 2, 1)
        # nn.init.xavier_uniform_(self.l5.weight, gain=1.0)
        if n_classes > 0:
            self.l_y = nn.Linear(256, n_classes)
            nn.init.xavier_uniform_(self.l_y.weight, gain=1.0)

        # Inference over z
        self.conv1z = nn.Conv2d(z_dim, 256, 1, stride=1, bias=False) # ch * 2 or bigger
        self.conv2z = nn.Conv2d(256, 256, 1, stride=1, bias=False)

        # Joint inference
        self.conv1xz = nn.Conv2d(512, 512, 1, stride=1, bias=False)
        self.conv2xz = nn.Conv2d(512, 512, 1, stride=1, bias=False)
        self.conv3xz = nn.Conv2d(512, 1, 1, stride=1, bias=False)

    def inf_z(self, z):
        z = F.dropout2d(F.leaky_relu(self.conv1z(z), negative_slope=0.1), 0.2)
        z = F.dropout2d(F.leaky_relu(self.conv2z(z), negative_slope=0.1), 0.2)
        return z

    def inf_xz(self, xz):
        xz = F.dropout(F.leaky_relu(self.conv1xz(xz), negative_slope=0.1), 0.2)
        xz = F.dropout(F.leaky_relu(self.conv2xz(xz), negative_slope=0.1), 0.2)
        return self.conv3xz(xz)

    def forward(self, x, z):
        z = z.unsqueeze(2).unsqueeze(2)
        h = x
        h = self.block1(h)
        h = self.block2(h)
        h = self.block3(h)
        h = self.block4(h)
        h = self.activation(h)
        h = h.view(h.size(0), h.size(1), -1)
        h = torch.sum(h, 2)
        w_y = self.l_y(h)
        h = h.unsqueeze(2).unsqueeze(2)
        z = self.inf_z(z)
        xz = torch.cat((h, z), dim=1)
        out = self.inf_xz(xz)
        out = torch.sigmoid(out).squeeze().squeeze()
        return out, w_y


class CombRobDisc(DiscMLP):
    def __init__(self, mlp_dim, num_cls, *args, **kwargs):
        super().__init__(mlp_dim=mlp_dim, *args, **kwargs)
        self.cls_linear = nn.Linear(in_features=mlp_dim, out_features=num_cls)

    def forward(self, img, latent):
        x = torch.cat([img, latent], dim=-1)
        y = self.mpls(x)
        y_cls = self.cls_linear(y)
        y = self.linear(y)
        return y, y_cls

    @classmethod
    def from_config(cls, config):
        return cls(
            num_cls=config.num_cls,
            n_blocks=config.comb_disc_blocks,
            mlp_dim=config.comb_disc_mlp_dim,
            in_dim=config.latent_disc_mlp_dim + config.disc_mult_chs["post"][-1],
            dropout=config.dropout,
            w_init=config.w_init,
            sn=config.spectral_norm,
        )
