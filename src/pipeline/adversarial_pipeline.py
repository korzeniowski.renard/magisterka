import matplotlib.pyplot as plt
import torch
from tqdm import tqdm
from pathlib import Path
import numpy as np
import torchvision.utils as vutils

from src.data_processing import data_loading
from src.model import architecture
from src.model import classifiers
from src.model import adversarial
from src.pipeline import logger as training_logger
from src.training_utils import training_utils


class RobPGDAdversarialAttackPipeline:
    def __init__(self, model, adv_attacker, criterion, dataloader, config):
        self.model = model
        self.adv_attacker = adv_attacker
        self.criterion = criterion
        self.dataloader = dataloader
        self.config = config

    def train_model(self):
        acc = self.perform_attack()
        print(f"Rob attacked acc {acc}")

    def perform_attack(self):
        running_acc = 0
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(self.config.device), y.to(self.config.device)
            x_adv = self.adv_attacker(x, y, model=self.model, criterion=self.criterion)
            y_pred = self.model(x_adv).cpu()
            running_acc += self.calculate_acc(preds=y_pred, labels=y)
        acc = running_acc / (step + 1)
        return acc

    def calculate_acc(self, preds, labels):
        preds = preds.max(dim=1)[1].cpu().numpy()
        labels = labels.cpu().numpy()
        return sum(preds == labels) / len(labels)

    @classmethod
    def from_repr_checkpoint(cls, data_path, config):
        config.device = torch.device(config.device)
        repr_model = architecture.RobBigBiGAN.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.repr_checkpoint_path)
        repr_model.load_state_dict(checkpoint, strict=True)
        repr_model = repr_model.encoder
        repr_model = repr_model.cuda()
        repr_model = repr_model.eval()

        dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="train")
        classifier = classifiers.ClassifierHead.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.clf_head_checkpoint_path)
        classifier.load_state_dict(checkpoint, strict=True)
        classifier = classifier.cuda()
        criterion = torch.nn.CrossEntropyLoss()

        model = architecture.ReprClf(head=classifier, repr_model=repr_model)

        adv_attacker = adversarial.AdvAttackerClfRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        return cls(
            model=model,
            adv_attacker=adv_attacker,
            criterion=criterion,
            dataloader=dataloader,
            config=config,
        )

    @classmethod
    def from_pytorch(cls, data_path, config):
        config.device = torch.device(config.device)
        model = classifiers.ResNet.from_config(config)
        checkpoint = torch.load(config.classifier_checkpoint_path)
        model.load_state_dict(checkpoint, strict=True)
        model = model.cuda()
        model = model.eval()

        dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="test")
        criterion = torch.nn.CrossEntropyLoss()

        adv_attacker = adversarial.AdvAttackerClfRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        return cls(
            model=model,
            adv_attacker=adv_attacker,
            criterion=criterion,
            dataloader=dataloader,
            config=config,
        )

    @classmethod
    def from_checkpoint(cls, data_path, config):
        config.device = torch.device(config.device)
        model = architecture.RobBigBiGAN.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.repr_checkpoint_path)
        model.load_state_dict(checkpoint, strict=True)
        model = model.encoder
        model = model.cuda()
        model = model.eval()

        dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="train")
        criterion = torch.nn.CrossEntropyLoss()

        adv_attacker = adversarial.AdvAttackerClfRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        return cls(
            model=model,
            adv_attacker=adv_attacker,
            criterion=criterion,
            dataloader=dataloader,
            config=config,
        )


class RobustRobClfPipeline:
    def __init__(self, repr_model, classifier, adv_attacker, criterion,
                 optimizer_repr, optimizer_clf, adv_criterion, dataloader, test_dataloader, logger, config):
        self.repr_model = repr_model
        self.adv_attacker = adv_attacker
        self.adv_criterion = adv_criterion
        self.classifier = classifier
        self.criterion = criterion
        self.optimizer_repr = optimizer_repr
        self.optimizer_clf = optimizer_clf
        self.dataloader = dataloader
        self.test_dataloader = test_dataloader
        self.logger = logger
        self.config = config

    def train_model(self):
        for epoch in range(self.config.epochs):
            self.run_epoch(epoch)
            self.run_epoch_test(epoch)

    def run_epoch(self, epoch):
        sum_acc = 0
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(self.config.device), y.to(self.config.device)
            x_adv = self.adv_attacker(x, y, model=self.classifier, criterion=self.criterion, lat_enc=self.repr_model)
            latent = self.repr_model(x_adv)
            y_hat = self.classifier(latent)

            self.classifier.zero_grad()
            loss = self.criterion(y_hat, y)
            loss.backward()
            self.optimizer_repr.step()
            self.optimizer_clf.step()

            acc = (y == y_hat.max(dim=1)[1]).float().mean()
            sum_acc += acc
            logging_dict = {"acc": sum_acc}
            self.logger(epoch, loss, step, logging_dict=logging_dict)
        print(f"acc {sum_acc / len(self.dataloader)}")
        self.save_model(epoch)

    def run_epoch_test(self, epoch):
        sum_acc = 0
        for step, (x, y) in tqdm(enumerate(self.test_dataloader)):
            x, y = x.to(self.config.device), y.to(self.config.device)
            x_adv = self.adv_attacker(x, y, model=self.classifier, criterion=self.criterion, lat_enc=self.repr_model)
            latent = self.repr_model(x_adv)
            y_hat = self.classifier(latent)

            acc = (y == y_hat.max(dim=1)[1]).float().mean()
            sum_acc += acc
        print(f"test acc {sum_acc / len(self.dataloader)}")
        self.save_model(epoch)

    def calculate_acc(self, preds, labels):
        preds = preds.max(dim=1)[1].cpu().numpy()
        labels = labels.cpu().numpy()
        return sum(preds == labels) / len(labels)

    def save_model(self, epoch):
        if (epoch % self.config.save_model_interval == 0) and epoch:
            save_folder = Path(self.config.save_model_path.format(
                    ds_name=self.config.ds_name,
                    model_architecture=self.config.model_architecture,
                    hparams=self.config.hparams_str,
            ))
            save_folder.mkdir(parents=True, exist_ok=True)
            save_path = str(save_folder / f"checkpoint_robust_classifier_{epoch}.pth")
            torch.save(self.classifier.state_dict(), save_path)

    @classmethod
    def from_checkpoint(cls, arch, data_path, config):
        config.device = torch.device(config.device)
        repr_model = arch.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.repr_checkpoint_path)
        repr_model.load_state_dict(checkpoint, strict=True)
        repr_model = repr_model.encoder
        repr_model = repr_model.cuda()
        repr_model = repr_model.eval()

        dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="train")
        test_dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config,
                                                                       ds_type="test")
        classifier = classifiers.ClassifierHead.from_config(config).to(device=config.device)
        criterion = torch.nn.CrossEntropyLoss()
        adv_criterion = torch.nn.CrossEntropyLoss()
        # optimizer = torch.optim.Adam(classifier.parameters(), lr=config.clf_lr, betas=config.betas)
        optimizer_repr = torch.optim.Adam(repr_model.parameters(), lr=config.clf_lr, betas=config.betas)
        optimizer_clf = torch.optim.Adam(classifier.parameters(), lr=config.clf_lr, betas=config.betas)
        logger = training_logger.ClfLogger.from_config(config=config, name=config.hparams_str)

        adv_attacker = adversarial.AdvAttackerClfRob(
            lr=config.rob_adv_lr,
            epsilon=config.rob_adv_epsilon,
            steps=config.rob_adv_steps,
            proportion=config.rob_proportion,
        )

        return cls(
            repr_model=repr_model,
            adv_criterion=adv_criterion,
            classifier=classifier,
            adv_attacker=adv_attacker,
            criterion=criterion,
            optimizer_repr=optimizer_repr,
            optimizer_clf=optimizer_clf,
            dataloader=dataloader,
            test_dataloader=test_dataloader,
            logger=logger,
            config=config,
        )


class PGDAdversarialAttackPipeline:
    """
    x -repr_model-> repr -adv_attack-> avd_repr -clf-> y
    """
    # attack only based on repr model grads
    def __init__(self, repr_model, classifier, adv_attacker, criterion, dataloader, config):
        self.repr_model = repr_model
        self.adv_attacker = adv_attacker
        self.classifier = classifier
        self.criterion = criterion
        self.dataloader = dataloader
        self.config = config

    def run_experiment(self):
        accs = []
        for eps in self.config.eps_list:
            acc = self.perform_attack(eps)
            accs.append(acc)
        self.save_img(accs)

    def save_img(self, accs):
        plt.plot(self.config.eps_list, accs, label="adv attack")
        file_name = f"adv_attacks_eps{self.config.eps_list}.png"
        gen_imgs_save_folder = Path(self.config.adv_attacks_imgs_save_path.format(
            ds_name=self.config.ds_name,
            model_architecture=self.config.model_architecture,
            hparams=self.config.hparams_str,
        ))
        gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
        gen_imgs_save_path = str(gen_imgs_save_folder / file_name)
        plt.savefig(fname=gen_imgs_save_path)

    def perform_attack(self, eps):
        running_acc = 0
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(self.config.device), y.to(self.config.device)
            x_adv = self.adv_attacker.projected_gradient_descent(x, y, eps, self.classifier, self.repr_model)
            latent = self.repr_model(x_adv)
            y_pred = self.classifier(latent).cpu()
            running_acc += self.calculate_acc(preds=y_pred, labels=y)
        acc = running_acc / (step + 1)
        return acc

    def calculate_acc(self, preds, labels):
        preds = preds.max(dim=1)[1].cpu().numpy()
        labels = labels.cpu().numpy()
        return sum(preds == labels) / len(labels)

    @classmethod
    def from_checkpoint(cls, data_path, config):
        config.device = torch.device(config.device)
        repr_model = architecture.BigBiGAN.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.repr_checkpoint_path)
        repr_model.load_state_dict(checkpoint, strict=True)
        repr_model = repr_model.encoder
        repr_model = repr_model.cuda()
        repr_model = repr_model.eval()

        dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="train")
        classifier = classifiers.ClassifierHead.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.clf_head_checkpoint_path)
        classifier.load_state_dict(checkpoint, strict=True)
        classifier = classifier.cuda()
        criterion = torch.nn.CrossEntropyLoss(reduce='sum')

        adv_attacker = adversarial.PGDAttacker(
            criterion=criterion,
            adv_num_steps=config.adv_num_steps,
            step_norm=config.step_norm,
            adv_step_size=config.adv_step_size,
            eps_norm=config.eps_norm,
            adv_clamp=config.adv_clamp
        )

        return cls(
            repr_model=repr_model,
            classifier=classifier,
            adv_attacker=adv_attacker,
            criterion=criterion,
            dataloader=dataloader,
            config=config,
        )


class AdvAttackGeneratorGAN:
    """
                            				    -> disc was attacked  -> recognition_loss
    imgs -> gen (possibele reg on max change) -|
                                                -> adv_input clf           -> adv_loss
    - how attacks look like
    - is disc able to classifie adv attacks from other sources
    """
    def __init__(
            self,
            databunch,
            gan,
            clf,
            adv_criterion,
            disc_criterion,
            gen_criterion,
            pixel_change_criterion,
            gen_optimizer,
            disc_optimizer,
            logger,
            eval_logger,
            config,
    ):
        self.databunch = databunch
        self.gan = gan
        self.clf = clf
        self.adv_criterion = adv_criterion
        self.disc_criterion = disc_criterion
        self.gen_criterion = gen_criterion
        self.pixel_change_criterion = pixel_change_criterion
        self.gen_optimizer = gen_optimizer
        self.disc_optimizer = disc_optimizer
        self.logger = logger
        self.eval_logger = eval_logger
        self.config = config
        self.counter = 0
        self.downsample = torch.nn.AvgPool2d(kernel_size=3, stride=2, padding=1)

    def train_model(self):
        for epoch in range(self.config.epochs):
            self.counter = 0
            self.run_train_epoch(epoch)
            self.run_eval_epoch(epoch)

    def run_train_epoch(self, epoch):
        for step, data in enumerate(self.databunch["train"]):
            (x1, y1), (x2, y2) = self.prepare_data(data)
            x2_adv, y2_adv, mask_adv = self.generate_adv_attack(x2, y2)
            self.gan.req_grad_disc(True)
            disc_loss, disc_real_acc, disc_fake_acc = self.forward_disc(x1, x2_adv, y1, y2)

            self.gan.req_grad_disc(False)
            gen_loss, gen_disc_acc, adv_loss, regularization_loss, adv_acc = self.forward_gen(x2_adv, y2_adv, mask_adv)

            y_hat = self.clf(x2)
            acc = (y2 == y_hat.max(dim=1)[1]).float().mean()

            self.save_img(epoch, x2, x2_adv)
            self.save_model(epoch)
            self.logger(epoch, step, disc_loss, gen_loss, gen_disc_acc,
                        disc_real_acc, disc_fake_acc, adv_loss.item(), regularization_loss, acc, adv_acc)

    def run_eval_epoch(self, epoch):
        correct, correct_adv = 0, 0
        for step, (x, y) in enumerate(self.databunch["test"]):
            with torch.no_grad():
                x, y = x.to(device=self.config.device), y.to(device=self.config.device)
                x = x if self.config.upsample_img else self.downsample(x)
                x_adv, y_adv, mask_adv = self.generate_adv_attack(x, y)
                y_hat = self.clf(x)
                y_hat_adv = self.clf(x_adv)
                adv_loss = -1 * self.adv_criterion(y_hat_adv, y)
                regularization_loss = 0
                adv_loss = adv_loss + regularization_loss

                correct += (y == y_hat.max(dim=1)[1]).float().sum()
                correct_adv += (y == y_hat_adv.max(dim=1)[1]).float().sum()

        clf_acc = correct / (len(self.databunch["test"]) * x.shape[0])
        clf_adv_acc = correct_adv / (len(self.databunch["test"]) * x.shape[0])
        self.eval_logger(epoch, clf_acc, adv_loss.item(), regularization_loss, clf_adv_acc)

    def adv_critertion(self, y_true, y_hat_adv):
        if self.config.directed_attack:
            # y_pred == y_adv
            pass
        else:
            loss = self.disc_criterion(y_true, y_hat_adv)
        return loss

    def prepare_data(self, data):
        x, y = data
        x, y = x.to(device=self.config.device), y.to(device=self.config.device)
        x = x if self.config.upsample_img else self.downsample(x)
        bs = x.shape[0]
        idxes = torch.randperm(bs)
        first_half, second_half = idxes[bs // 2:, ...], idxes[bs // 2:, ...]
        x1, x2 = x[first_half], x[second_half]
        y1, y2 = y[first_half], y[second_half]
        return (x1, y1), (x2, y2)

    def generate_adv_attack(self, x, y):
        # options a) change to any y and give info about current class, b) ask to change to specific y
        if self.config.directed_attack:
            y_adv = torch.randint(low=0, high=self.config.class_count, size=y.shape)
        else:
            y_adv = y
        noise = training_utils.truncated_normal((y_adv.shape[0], self.config.latent_dim)).to(device=self.config.device)
        mask_adv = self.gan.generator.forward(noise, y_adv)
        x_adv = x + mask_adv
        x_adv_max, _ = self.amax(x_adv, keep_dim=0)
        x_max, _ = self.amax(x, keep_dim=0)
        x_adv = x_adv / x_adv_max.view(-1, 1, 1, 1) * x_max.view(-1, 1, 1, 1)
        return x_adv, y_adv, mask_adv

    def amax(self, x, keep_dim):
        return x.view(x.size(keep_dim), -1).max(dim=-1)

    def forward_gen(self, x2_adv, real_y2, mask_adv): # class info in which generator attack is pushing would be usefull
        self.gan.generator.zero_grad()
        _, pred_gen_x = self.gan.discriminator(x=x2_adv, cls=real_y2)
        pred_gen_x = torch.sigmoid(pred_gen_x.reshape(-1))

        label_gen_img = torch.ones(pred_gen_x.shape[0], device=self.config.device)
        gen_loss = self.gen_criterion(pred_gen_x, label_gen_img)

        y_hat_adv = self.clf(x2_adv)
        adv_loss = -1 * self.adv_criterion(y_hat_adv, real_y2)
        adv_acc = (real_y2 == y_hat_adv.max(dim=1)[1]).float().mean()

        regularization_loss = \
            self.config.pixel_change_coeff * self.pixel_change_criterion(mask_adv, torch.zeros_like(mask_adv))

        comb_gen_loss = gen_loss + adv_loss + regularization_loss
        comb_gen_loss.backward()

        gen_disc_acc = 1 - pred_gen_x.mean().item()
        self.gen_optimizer.step()

        return gen_loss, gen_disc_acc, adv_loss, regularization_loss, adv_acc

    def forward_disc(self, real_x, x2_adv, real_y, real_y2):
        for _ in range(self.config.disc_steps):
            self.gan.discriminator.zero_grad()
            _, pred_real_x = self.gan.discriminator(x=real_x, cls=real_y)
            pred_real_x = torch.sigmoid(pred_real_x.reshape(-1))

            label_real_img = torch.ones(pred_real_x.shape[0], device=self.config.device)
            real_x_loss = self.disc_criterion(pred_real_x, label_real_img)
            real_x_loss.backward()

            _, pred_gen_x = self.gan.discriminator(x=x2_adv.detach(), cls=real_y2)
            pred_gen_x = torch.sigmoid(pred_gen_x.reshape(-1))

            label_gen_img = torch.zeros(pred_gen_x.shape[0], device=self.config.device)
            gen_x_loss = self.disc_criterion(pred_gen_x, label_gen_img)
            gen_x_loss.backward()

            disc_real_acc = pred_real_x.mean().item()
            disc_fake_acc = 1 - pred_gen_x.mean().item()

            disc_loss = gen_x_loss + real_x_loss

            self.disc_optimizer.step()

        return disc_loss, disc_real_acc, disc_fake_acc

    def save_model(self, epoch):
        if (epoch % self.config.save_model_interval == 0) and epoch:
            save_folder = Path(self.config.save_model_path.format(
                    ds_name=self.config.ds_name,
                    model_architecture=self.config.model_architecture,
                    hparams=self.config.hparams_str,
            ))
            save_folder.mkdir(parents=True, exist_ok=True)
            save_path = str(save_folder / f"checkpoint_{epoch}.pth")
            torch.save(self.gan.state_dict(), save_path)

    def save_img(self, epoch, real_img, img_gen, latent=None, y=None):
        if epoch % self.config.save_adv_img_interval == 0 and self.counter == 0:
            with torch.no_grad():
                #img_gen, noise = self.model.generate_imgs(fixed=True)
                fake = img_gen.detach().cpu()[:self.config.save_img_count, ...]
            fake_img = np.transpose(vutils.make_grid(
                fake, padding=2, nrow=self.config.img_rows, normalize=True), (1, 2, 0))
            plt.imshow(fake_img)

            file_name = f"ep{epoch}_step{self.counter}.png"
            gen_imgs_save_folder = Path(self.config.gen_imgs_save_path.format(
                ds_name=self.config.ds_name,
                model_architecture=self.config.model_architecture,
                hparams=self.config.hparams_str,
            ))
            gen_imgs_save_folder.mkdir(parents=True, exist_ok=True)
            gen_imgs_save_path = str(gen_imgs_save_folder / file_name)
            plt.savefig(fname=gen_imgs_save_path)

            real_img = real_img.cpu()[:self.config.save_img_count, ...]
            real_img = np.transpose(vutils.make_grid(
                real_img, padding=2, nrow=self.config.img_rows, normalize=True), (1, 2, 0))
            plt.imshow(real_img)
            real_imgs_save_folder = Path(self.config.real_imgs_save_path.format(
                ds_name=self.config.ds_name,
                model_architecture=self.config.model_architecture,
                hparams=self.config.hparams_str,
            ))
            real_imgs_save_folder.mkdir(parents=True, exist_ok=True)
            real_imgs_save_path = real_imgs_save_folder / file_name
            plt.savefig(
                fname=str(real_imgs_save_path),
            )

        self.counter += 1

    def calculate_acc(self):
        pass

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        databunch = data_loading.Databunch.from_config(config, data_path)
        gan = architecture.BigGAN.from_config(config).to(device=config.device)
        clf = classifiers.ResNet.from_config(config).to(device=config.device)
        clf_checkpoint = torch.load(config.cls_checkpoint_path)
        clf.load_state_dict(clf_checkpoint, strict=True)

        gen_criterion = torch.nn.BCELoss()
        disc_criterion = torch.nn.BCELoss()
        adv_criterion = torch.nn.CrossEntropyLoss()
        pixel_change_criterion = torch.nn.L1Loss() #MSELoss()

        gen_optimizer = torch.optim.Adam(gan.get_gen_params(), lr=config.lr_gen, betas=config.betas)
        disc_optimizer = torch.optim.Adam(gan.get_disc_params(), lr=config.lr_disc, betas=config.betas)

        logger = training_logger.AdvGANLogger.from_config(config=config, name=config.hparams_str)
        eval_logger = training_logger.EvalAdvGANLogger.from_config(config=config, name=config.hparams_str)
        return cls(
            databunch=databunch,
            gan=gan,
            clf=clf,
            adv_criterion=adv_criterion,
            gen_criterion=gen_criterion,
            disc_criterion=disc_criterion,
            pixel_change_criterion=pixel_change_criterion,
            gen_optimizer=gen_optimizer,
            disc_optimizer=disc_optimizer,
            logger=logger,
            eval_logger=eval_logger,
            config=config,
        )


class LatentAdvAttackClassifier:
    """
      x -repr-> repr -gen-> x_repr -pretrained_clf->
                                                   +-> nonagreement = input adv attacked
                                 x -pretrained_clf->
    - pretrained_clf trained on normal data
    - adv attack targeted on pretrained_clf (wont be pred as Y_adv only if noise is lost)
    - adv attack targeted on repr (head_clf)
    does it work on adv attacked not on pretrained_clf
    """
    # attack repr_encoder instead
    def __init__(self, dataloader, clf, adv_attacker, repr_model, gen_model, config):
        self.dataloader = dataloader
        self.clf = clf
        self.repr_model = repr_model
        self.gen_model = gen_model
        self.adv_attacker = adv_attacker
        self.config = config

    def train_model(self):
        results = dict()
        results["not_attacked"] = self.run_attack(attack=True)
        # {'acc': tensor(0.9764), 'adv_acc': tensor(0.7412), 'latent_match': tensor(0.7273)} # adv_acc seem too high (expecially after enc_gen)
        results["adv_attacked"] = self.run_attack(attack=False)
        # {'acc': tensor(0.9769), 'adv_acc': tensor(0.7567), 'latent_match': tensor(0.7407)}
        print(results)

    def run_attack(self, attack):
        metrics = {"acc": 0.0, "adv_acc": 0.0, "latent_match": 0.0}
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(device=self.config.device), y.to(device=self.config.device)
            x_adv = self.adv_attack(x, y) if attack else x
            x_repr = self.encode_gen_decode(x_adv, y)
            y_hat = self.clf(x)
            y_hat_adv = self.clf(x_repr)
            acc, adv_acc, latent_match = self.calc_metrics(y, y_hat, y_hat_adv)
            self.update_metrics(acc, adv_acc, latent_match, metrics)
            print({k: v / (step + 1) for k, v in metrics.items()})
        metrics = {k: v / (step + 1) for k, v in metrics.items()}
        return metrics

    def adv_attack(self, x, y):
        x = self.adv_attacker.projected_gradient_descent(x, y, self.config.eps, self.clf)
        return x

    def encode_gen_decode(self, x, y):
        repr = self.repr_model(x)
        x_repr = self.gen_model(repr, y)
        return x_repr

    def calc_metrics(self, y, y_hat, y_hat_adv):
        # # with attack
        # y_hat != y_hat_adv # ~1
        # y == y_hat # ~1
        # y == y_hat_adv # ~0
        # # without an attack
        # y_hat != y_hat_adv  # ~0
        # y == y_hat  # ~1
        # y == y_hat_adv  # ~1
        acc = self.calculate_acc(preds=y_hat, labels=y)
        adv_acc = self.calculate_acc(preds=y_hat_adv, labels=y)
        latent_match = self.calculate_acc(preds=y_hat_adv, labels=y_hat, reduce_label=True)
        return acc, adv_acc, latent_match

    def calculate_acc(self, preds, labels, reduce_label=False):
        preds = preds.max(dim=1)[1].cpu()
        labels = labels.max(dim=1)[1] if reduce_label else labels
        labels = labels.cpu()
        return (preds == labels).float().mean()

    def update_metrics(self, acc, adv_acc, latent_match, metrics):
        metrics["acc"] += acc
        metrics["adv_acc"] += adv_acc
        metrics["latent_match"] += latent_match

    @classmethod
    def from_config(cls, data_path, config):
        config.device = torch.device(config.device)
        databunch = data_loading.Databunch.from_config(config, data_path)

        repr_model = architecture.BigBiGAN.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.repr_checkpoint_path)
        repr_model.load_state_dict(checkpoint, strict=True)
        gen_model = repr_model.generator
        gen_model = gen_model.cuda()
        gen_model = gen_model.eval()

        repr_model = repr_model.encoder
        repr_model = repr_model.cuda()
        repr_model = repr_model.eval()

        clf = classifiers.ResNet.from_config(config).to(device=config.device)
        clf_checkpoint = torch.load(config.clf_checkpoint_path)
        clf.load_state_dict(clf_checkpoint, strict=True)
        clf = clf.cuda()
        criterion = torch.nn.CrossEntropyLoss(reduce='sum')

        adv_attacker = adversarial.PGDAttacker(
            criterion=criterion,
            adv_num_steps=config.adv_num_steps,
            step_norm=config.step_norm,
            adv_step_size=config.adv_step_size,
            eps_norm=config.eps_norm,
            adv_clamp=config.adv_clamp
        )

        return cls(
            dataloader=databunch['train'],
            repr_model=repr_model,
            gen_model=gen_model,
            clf=clf,
            adv_attacker=adv_attacker,
            config=config,
        )


class RobustClfPipeline:
    def __init__(self, repr_model, classifier, adv_attacker, criterion,
                 optimizer, adv_criterion, dataloader, logger, config):
        self.repr_model = repr_model
        self.adv_attacker = adv_attacker
        self.adv_criterion = adv_criterion
        self.classifier = classifier
        self.criterion = criterion
        self.optimizer = optimizer
        self.dataloader = dataloader
        self.logger = logger
        self.config = config

    def train_model(self):
        for epoch in range(self.config.epochs):
            self.run_epoch(epoch)

    def run_epoch(self, epoch):
        acc = 0
        for step, (x, y) in tqdm(enumerate(self.dataloader)):
            x, y = x.to(self.config.device), y.to(self.config.device)
            x_adv = self.adv_attack(x, y)
            latent = self.repr_model(x_adv)
            y_hat = self.classifier(latent).cpu()

            self.classifier.zero_grad()
            loss = self.criterion(y_hat, y)
            loss.backward()
            self.optimizer.step()

            acc += (y == y_hat.max(dim=1)[1]).float().mean()
            logging_dict = {"acc": acc}
            self.logger(epoch, loss, step, logging_dict=logging_dict)
        print(f"acc {acc / len(self.dataloader)}")
        self.save_model(epoch)

    def adv_attack(self, x, y):
        return self.adv_attacker.projected_gradient_descent(x, y, self.config.eps, self.classifier, self.repr_model)

    def calculate_acc(self, preds, labels):
        preds = preds.max(dim=1)[1].cpu().numpy()
        labels = labels.cpu().numpy()
        return sum(preds == labels) / len(labels)

    def save_model(self, epoch):
        if (epoch % self.config.save_model_interval == 0) and epoch:
            save_folder = Path(self.config.save_model_path.format(
                    ds_name=self.config.ds_name,
                    model_architecture=self.config.model_architecture,
                    hparams=self.config.hparams_str,
            ))
            save_folder.mkdir(parents=True, exist_ok=True)
            save_path = str(save_folder / f"checkpoint_robust_classifier_{epoch}.pth")
            torch.save(self.classifier.state_dict(), save_path)

    @classmethod
    def from_checkpoint(cls, data_path, config):
        config.device = torch.device(config.device)
        repr_model = architecture.BigBiGAN.from_config(config).to(device=config.device)
        checkpoint = torch.load(config.repr_checkpoint_path)
        repr_model.load_state_dict(checkpoint, strict=True)
        repr_model = repr_model.encoder
        repr_model = repr_model.cuda()
        repr_model = repr_model.eval()

        dataloader = data_loading.get_supported_loader(config.ds_name)(data_path=data_path, config=config, ds_type="train")
        classifier = classifiers.ClassifierHead.from_config(config).to(device=config.device)
        criterion = torch.nn.CrossEntropyLoss()
        adv_criterion = torch.nn.CrossEntropyLoss(reduce='sum')
        optimizer = torch.optim.Adam(classifier.parameters(), lr=config.clf_lr, betas=config.betas)
        logger = training_logger.ClfLogger.from_config(config=config, name=config.hparams_str)

        adv_attacker = adversarial.PGDAttacker(
            criterion=criterion,
            adv_num_steps=config.adv_num_steps,
            step_norm=config.step_norm,
            adv_step_size=config.adv_step_size,
            eps_norm=config.eps_norm,
            adv_clamp=config.adv_clamp
        )

        return cls(
            repr_model=repr_model,
            adv_criterion=adv_criterion,
            classifier=classifier,
            adv_attacker=adv_attacker,
            criterion=criterion,
            optimizer=optimizer,
            dataloader=dataloader,
            logger=logger,
            config=config,
        )
